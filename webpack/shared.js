const webpack = require('webpack');
const nodeExternals = require('webpack-node-externals');

if (!process.env.NODE_ENV) {
  process.env.NODE_ENV = 'development';
}

const isProduction = process.env.NODE_ENV === 'production';
const sentryToken = process.env.SENTRY_DSN || '';
const releaseName = process.env.CI_COMMIT_REF_NAME || 'dev';
const apiURL = process.env.API_URL || (isProduction ? 'https://api.zaber.io' : 'https://api-staging.zaber.io');

if (isProduction && !sentryToken) {
  throw new Error('SENTRY_DSN env variable not set for production build');
}

const shared = {
  mode: process.env.NODE_ENV,
  devtool: 'source-map',
  externals: [function (context, request, callback) {
    if (request.match(/^(fs|path)$/)) {
      return callback(null, 'commonjs ' + request);
    }
    callback();
  }, nodeExternals()],
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(process.env.NODE_ENV),
      'process.env.SENTRY_DSN': JSON.stringify(sentryToken),
      'process.env.RELEASE_NAME': JSON.stringify(releaseName),
      'process.env.API_URL': JSON.stringify(apiURL),
      'process.env.JEST_WORKER_ID': JSON.stringify(''),
    }),
  ],
};


module.exports = {
  shared,
};
