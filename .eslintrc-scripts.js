module.exports = {
  'env': {
    'es6': true,
    'node': true
  },
  'extends': 'eslint:recommended',
  'parserOptions': {
    'ecmaVersion': 2018
  },
  'rules': {
    'max-len': ['error', { 'code': 120 }],
    'indent': [
      'error',
      2
    ],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'camelcase': [
      'error'
    ],
    'prefer-const': [
      'error'
    ],
    'no-var': [
      'error'
    ],
    'object-curly-spacing': [
      'error',
      'always'
    ],
    'arrow-parens': [
      'error',
      'as-needed',
    ],
    'prefer-arrow-callback': [
      'error'
    ],
    'no-multiple-empty-lines': [
      'error'
    ],
    'no-cond-assign': [
      'error',
      'except-parens'
    ],
  }
};
