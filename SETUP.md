For developers who want to run the app from source:
* Install node.js LTS
* Run `npm i`
* Run `npm run watch` in one window to compile the JS
* Run `npm start` in second window to launch the electron app
