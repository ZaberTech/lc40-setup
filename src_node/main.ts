import { app, BrowserWindow, dialog } from 'electron';
import path from 'path';

import { environment } from '../src/environment';
import { initErrors, errorEmitter, handleError } from '../src/errors/errors';

initErrors();
errorEmitter.once('error', afterError);

const WAIT_FOR_SENTRY_TO_REPORT = 5000;

function afterError(): void {
  setTimeout(() => {
    dialog.showMessageBox({
      title: 'Error',
      message: [
        'Error has occurred and the application must be closed.',
        'The error was automatically reported.',
        'If the issue persists, please contact Zaber support at https://www.zaber.com/contact.',
      ].join(' '),
    }).catch(() => null);
    process.exit(1);
  }, WAIT_FOR_SENTRY_TO_REPORT);
}

async function createWindow (): Promise<void> {
  const mainWindow = new BrowserWindow({
    width: 1280,
    height: 880 + (!environment.isProduction ? 60 : 0),
    minWidth: 1000,
    minHeight: 670,
    webPreferences: {
      nodeIntegration: true,
      enableRemoteModule: true,
    },
    frame: !environment.isProduction || process.platform !== 'win32',
  });
  mainWindow.setMenuBarVisibility(!environment.isProduction);
  await mainWindow.loadFile(path.join(__dirname, '../index.html'));

  if (!environment.isProduction) {
    mainWindow.webContents.openDevTools();
  }
}

async function main(): Promise<void> {
  await app.whenReady();
  await createWindow();

  app.on('activate', () => {
    // On macOS it's common to re-create a window in the app when the
    // dock icon is clicked and there are no other windows open.
    if (BrowserWindow.getAllWindows().length === 0) {
      createWindow().catch(handleError);
    }
  });

  // Quit when all windows are closed.
  app.on('window-all-closed', () => {
    if (!environment.isProduction) {
      createWindow().catch(handleError);
      return;
    }

    // On macOS it is common for applications and their menu bar
    // to stay active until the user quits explicitly with Cmd + Q
    if (process.platform !== 'darwin') { app.quit(); }
  });
}

main().catch(err => {
  handleError(err);
});
