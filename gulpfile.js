/* eslint-disable camelcase,no-console */
const path = require('path');
const fs = require('fs').promises;
const packageFile = require('./package.json');
const { isRelease, exec } = require('./scripts/utils');

const prepare_code_signing_win = async () => {
  const envFile = path.join(__dirname, 'dist_env.ps1');

  if (!isRelease) {
    await fs.writeFile(envFile, `
Write-Host "Not a release"
`.trim());

    return;
  }

  if (!process.env.CSC_KEY_PASSWORD) {
    throw new Error('CSC_KEY_PASSWORD is not set');
  }

  await exec([
    'git clone --depth 1 --branch',
    process.env.TOOLS_BRANCH || 'master',
    `${process.env.GIT_SERVER}/software-internal/software-keys.git keys`
  ].join(' '));

  await fs.writeFile(envFile, `
Write-Host "Setting signing variables"
$env:CSC_LINK="${path.join(__dirname, 'keys', 'keys', 'apps', 'codeSign.p12')}"
  `.trim(), 'utf8');
};

const publish = async () => {
  const fileList = [
    `Zaber LC40 Setup Installer-${packageFile.version}.exe`,
    `Zaber LC40 Setup-${packageFile.version}.dmg`,
    `zaber-lc40-setup_${packageFile.version}_amd64.deb`,
  ];
  const toFolder = `~/web-x64/website/source/software/lc40-setup/${packageFile.version}/`;
  for (const fileName of fileList) { // eslint-disable-line no-unused-vars
    await exec(`rsync -a "dist/${fileName}" zaber@zaber.nmsrv.com:${toFolder}`);
  }
};

module.exports = {
  prepare_code_signing_win,
  publish,
};
