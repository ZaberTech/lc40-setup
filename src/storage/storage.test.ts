import 'reflect-metadata';
import { Container, injectable } from 'inversify';

import { createContainer, destroyContainer } from '../container';
import { Storage, LocalStorageSymbol, StorageKeys } from '../storage';

let container: Container;
let storageMock: StorageMock;

@injectable()
class StorageMock {
  public data: { [key:string]: string } = {};

  public setItem = jest.fn((key: string, value: string) => this.data[key] = value);
  public getItem = jest.fn((key: string) => this.data[key] || null);
}

let storage: Storage;

beforeEach(() => {
  container = createContainer();
  container.rebind<unknown>(LocalStorageSymbol).to(StorageMock);
  storageMock = (container.get(LocalStorageSymbol) as unknown) as StorageMock;

  storage = container.get(Storage);
});

afterEach(() => {
  destroyContainer();
});

test('stores provided value', () => {
  storage.save(StorageKeys.SHOW_SPLASH_SCREEN, { x: 3 });

  expect(storageMock.data).toEqual({
    SHOW_SPLASH_SCREEN: '{"x":3}',
  });

  const value = storage.load(StorageKeys.SHOW_SPLASH_SCREEN);
  expect(value).toEqual({ x: 3 });
});

test('stores string value', () => {
  storage.save(StorageKeys.SHOW_SPLASH_SCREEN, 'test_value');
  const value = storage.load(StorageKeys.SHOW_SPLASH_SCREEN);
  expect(value).toEqual('test_value');
});

test('returns default value when null is retrieved', () => {
  const value = storage.load(StorageKeys.SHOW_SPLASH_SCREEN, 'default');
  expect(value).toBe('default');
});

test('returns default value when value is not JSON parsable', () => {
  storageMock.data[StorageKeys.SHOW_SPLASH_SCREEN] = '{';
  const value = storage.load(StorageKeys.SHOW_SPLASH_SCREEN, { a: 1 });
  expect(value).toEqual({ a: 1 });
});
