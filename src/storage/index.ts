import { injectable, inject } from 'inversify';
import _ from 'lodash';

import { Log, Logger } from '../log';

export enum StorageKeys {
  SHOW_SPLASH_SCREEN = 'SHOW_SPLASH_SCREEN',
}

export const LocalStorageSymbol = Symbol('localStorage');

@injectable()
export class Storage {
  private log: Logger;

  constructor(
    @inject(LocalStorageSymbol) private localStorage: WindowLocalStorage['localStorage'],
    @inject(Log) log: Log,
  ) {
    this.log = log.getLogger('storage');
  }

  public save<T>(key: StorageKeys, value: T): void {
    if (typeof value === 'undefined') {
      value = null;
    }

    this.localStorage.setItem(key, JSON.stringify(value));
  }

  public load<T>(key: StorageKeys, defaultValue: T = null): T {
    const raw: string = this.localStorage.getItem(key);
    if (raw === null) {
      return _.cloneDeep(defaultValue);
    }

    try {
      return JSON.parse(raw) as T;
    } catch (err) {
      this.log.warn(`Cannot deserialize ${key}: ${raw}`, err);
      return _.cloneDeep(defaultValue);
    }
  }
}
