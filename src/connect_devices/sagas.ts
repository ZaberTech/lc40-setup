import { all, takeLatest, put, call, race, take, delay, select } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import _ from 'lodash';
import { ascii, DeviceAddressConflictException, NoDeviceFoundException, MotionLibException } from '@zaber/motion';

import { getContainer } from '../container';
import { ZML } from '../zml';
import { environment } from '../environment';
import { Action } from '../utils';

import { ActionTypes, actions, ActionsToPayloads } from './actions';
import { selectPorts, selectSelectedPort, selectIsConnected } from './selectors';
import { Steps, OpenPortErrorType } from './types';
import { zmlDevicesToDevices } from './utils';

const LIST_PORT_LOOP_DELAY = 500;

export function* connectDevicesSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.START_MONITORING_PORTS, startMonitoringPorts),
    takeLatest(ActionTypes.DETECT_DEVICES, detectDevices),
    takeLatest(ActionTypes.PORT_OPEN, monitorConnectedPort),
    takeLatest(ActionTypes.CLOSE_PORT, closePort),
    takeLatest(ActionTypes.CHANGE_PORT, changePort),
  ]);
}

function* startMonitoringPorts(): SagaIterator {
  yield race({
    loop: call(monitorPorts),
    cancel: take(ActionTypes.STOP_MONITORING_PORTS),
  });
}

function* monitorPorts(): SagaIterator {
  const zml = getContainer().get<ZML>(ZML);

  while (true) {
    const ports: string[] = yield call([zml, zml.listPorts]);
    ports.sort();
    if (!environment.isRelease && !environment.isTest) {
      ports.push('tcp://localhost:11321');
      ports.push('tcp://192.168.149.51:11321');
    }

    const currentPorts: ReturnType<typeof selectPorts> = yield select(selectPorts);
    if (!currentPorts) {
      yield put(actions.portsChanged(ports));
    } else {
      const addedPorts = _.difference(ports, currentPorts);
      const removePorts = _.difference(currentPorts, ports);

      if (addedPorts.length > 0 || removePorts.length > 0) {
        yield put(actions.portsChanged(ports));
        if (addedPorts.length > 0) {
          yield put(actions.newPortConnected(addedPorts[0]));
        }
      }
    }

    yield delay(LIST_PORT_LOOP_DELAY);
  }
}

function* detectDevices(): SagaIterator {
  const zml = getContainer().get<ZML>(ZML);

  try {
    const isConnected: boolean = yield select(selectIsConnected);
    if (!isConnected) {
      const selectedPort: ReturnType<typeof selectSelectedPort> = yield select(selectSelectedPort);
      yield call([zml, zml.openSerialPort], selectedPort);
      yield put(actions.portOpen());
    }

    let devices: ascii.Device[];
    try {
      devices = yield call([zml.connection, zml.connection.detectDevices]);
    } catch (err) {
      if (err instanceof DeviceAddressConflictException) {
        yield call([zml.connection, zml.connection.renumberDevices]);
        devices = yield call([zml.connection, zml.connection.detectDevices]);
      } else if (err instanceof NoDeviceFoundException) {
        devices = [];
      } else {
        throw err;
      }
    }

    const deviceInfo = zmlDevicesToDevices(devices);

    yield put(actions.devicesDetected(deviceInfo));
  } catch (err) {
    if (err instanceof MotionLibException) {
      let type = OpenPortErrorType.GENERIC;
      if (err.message.includes('Port is likely already opened')) {  // TODO: replace by custom exception ZML-446
        type = OpenPortErrorType.PORT_BUSY;
      }

      yield put(actions.openPortErr({ type, message: String(err) }));
    } else {
      throw err;
    }
  }
}

function* monitorConnectedPort(): SagaIterator {
  const zml = getContainer().get<ZML>(ZML);

  const result = yield race({
    disconnected: call(() => zml.connection.disconnected.toPromise()),
    closed: take(ActionTypes.CLOSE_PORT),
  });

  if (result.disconnected) {
    yield put(actions.closePort(true));
  }
}

function* closePort(action: Action<ActionsToPayloads[ActionTypes.CLOSE_PORT]>): SagaIterator {
  const zml = getContainer().get<ZML>(ZML);

  yield call([zml, zml.close]);

  if (action.value.forceFullyDisconnected) {
    yield put(actions.navigateToStep(Steps.SELECT_PORT));
  }
}

function* changePort(): SagaIterator {
  yield put(actions.closePort(false));
  yield put(actions.navigateToStep(Steps.SELECT_PORT));
}
