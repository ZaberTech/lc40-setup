import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectConnectedDevices } from '../store';
import { nullGuard } from '../utils';

export const selectPorts = createSelector(selectConnectedDevices, state => state.ports);
export const selectSelectedPort = createSelector(selectConnectedDevices, state => state.selectedPort);
export const selectPortFoundByConnecting = createSelector(selectConnectedDevices, state => state.portFoundByConnecting);
export const selectDevices = createSelector(selectConnectedDevices, state => state.devices);
export const selectDetectingDevices = createSelector(selectConnectedDevices, state => state.detectingDevices);
export const selectOpenPortErr = createSelector(selectConnectedDevices, state => state.openPortErr);
export const selectIsConnected = createSelector(selectConnectedDevices, state => state.isConnected);
export const selectWasForcefullyDisconnected = createSelector(selectConnectedDevices, state => state.wasForcefullyDisconnected);
export const selectDeviceAxes = createSelector(selectDevices, nullGuard(
  devices => _.flatten(devices.map(device => device.axes.map(axis => ({ device, axis }))))
));
export const selectCompatibleDeviceAxes = createSelector(selectDeviceAxes, nullGuard(
  deviceAxes => deviceAxes.filter(deviceAxis => deviceAxis.axis.isCompatible)
));
export const selectHasCompatibleDevices = createSelector(selectCompatibleDeviceAxes, devices => devices && devices.length > 0);
