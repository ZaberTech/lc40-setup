import React from 'react';
import { injectable, Container } from 'inversify';
import { render, RenderResult } from '@testing-library/react';
import { ReplaySubject } from 'rxjs';
import { DeviceAddressConflictException, ascii, ConnectionFailedException } from '@zaber/motion';

import '../test/mocks/Select';
import { waitTick, wrapWithNewStore, waitUntil, Defer, defer, wrapWithRouter } from '../test';

import { createContainer, destroyContainer } from '../container';
import { ZML } from '../zml';

import { DetectDevices } from './DetectDevices';

const TestDetectDevices = wrapWithNewStore(wrapWithRouter(DetectDevices));

jest.useFakeTimers();

class AxisMock {
  get identity(): Partial<ascii.AxisIdentity> {
    return { isPeripheral: false };
  }
}

class DeviceMock {
  private axes: AxisMock[];
  constructor(public readonly deviceAddress: number) {
    this.axes = [new AxisMock()];
  }

  get identity(): Partial<ascii.DeviceIdentity> {
    return { deviceId: 1235, axisCount: 1, name: 'X-TEST' };
  }

  public getAxis(axisNumber: number): AxisMock {
    return this.axes[axisNumber - 1];
  }
}

let addressConflict = false;
class ConnectionMock {
  private devices: DeviceMock[];
  public disconnected = new ReplaySubject();
  public renumberDevices: jest.Mock;

  constructor(public readonly port: string) {
    this.devices = [new DeviceMock(1)];

    this.renumberDevices = jest.fn(() => Promise.resolve(1));
  }

  public async detectDevices(): Promise<DeviceMock[]> {
    if (addressConflict) {
      addressConflict = false;
      throw new DeviceAddressConflictException('conflict');
    }
    return this.devices;
  }
}

let waitForPort: Defer<void>;
let zmlMock: ZmlMock;

@injectable()
class ZmlMock {
  public connection: ConnectionMock;
  public close: jest.Mock;

  constructor() {
    zmlMock = this;
    this.openSerialPort = this.openSerialPort.bind(this);
    this.close = jest.fn(() => Promise.resolve());
  }

  public async openSerialPort(port: string): Promise<void> {
    waitForPort = defer();
    try {
      await waitForPort.promise;
    } finally {
      waitForPort = null;
    }
    this.connection = new ConnectionMock(port);
  }
}

let container: Container;

beforeAll(() => {
  container = createContainer();
  container.bind<unknown>(ZML).to(ZmlMock);
});

afterAll(() => {
  destroyContainer();
});

let wrapper: RenderResult;

beforeEach(() => {
  wrapper = render(<TestDetectDevices />);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null;
});

test('opens port and detects devices', async () => {
  wrapper.getByText(/Detecting devices/);

  await waitUntil(() => !!waitForPort);
  waitForPort.resolve();

  await wrapper.findByText(/X-TEST/);
});

test('displays error if it cannot open the port', async () => {
  await waitUntil(() => !!waitForPort);
  waitForPort.reject(new ConnectionFailedException('Port is broken'));

  await wrapper.findByText(/Cannot open the port\./);
  wrapper.getByText(/Port is broken/);
});

test('displays error if it cannot open the port and the port is busy', async () => {
  await waitUntil(() => !!waitForPort);
  waitForPort.reject(new ConnectionFailedException('Port is likely already opened by another'));

  await wrapper.findByText(/Make sure no other application \(e\.g\. Zaber Console\) is using the port/);
});

test('renumbers devices if there is a conflict', async () => {
  addressConflict = true;

  await waitUntil(() => !!waitForPort);
  waitForPort.resolve();

  await wrapper.findByText(/X-TEST/);

  expect(zmlMock.connection.renumberDevices).toHaveBeenCalledTimes(1);
});

test('disconnection redirects to the select port', async () => {
  await waitUntil(() => !!waitForPort);
  waitForPort.resolve();

  await wrapper.findByText(/Devices/);

  zmlMock.connection.disconnected.next(new ConnectionFailedException('Disconnected'));
  zmlMock.connection.disconnected.complete();

  await waitTick(1);

  expect(zmlMock.close).toBeCalledTimes(1);

  const state = TestDetectDevices.testStore.getState();
  expect(state.connectDevices.wasForcefullyDisconnected).toBe(true);
});
