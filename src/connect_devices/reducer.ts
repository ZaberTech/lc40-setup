import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { Device, OpenPortError } from './types';

export interface State {
  ports: string[];
  portFoundByConnecting: string;
  selectedPort: string;
  detectingDevices: boolean;
  devices: Device[];
  openPortErr: OpenPortError;
  isConnected: boolean;
  wasForcefullyDisconnected: boolean;
}

const initialState: State = {
  ports: null,
  portFoundByConnecting: null,
  selectedPort: null,
  detectingDevices: false,
  devices: null,
  openPortErr: null,
  isConnected: false,
  wasForcefullyDisconnected: false,
};

const portsChanged = (state: State, { ports }: ActionsToPayloads[ActionTypes.PORTS_CHANGED]): State =>
  ({
    ...state,
    ports,
    selectedPort: ports.includes(state.selectedPort) ? state.selectedPort : null,
    portFoundByConnecting: ports.includes(state.portFoundByConnecting) ? state.portFoundByConnecting : null,
  });
const newPortConnected = (state: State, { port }: ActionsToPayloads[ActionTypes.NEW_PORT_CONNECTED]): State =>
  !state.isConnected ? ({
    ...state,
    portFoundByConnecting: port,
    selectedPort: port,
  }) : state;
const selectedPortChanged = (state: State, { newPort }: ActionsToPayloads[ActionTypes.SELECTED_PORT_CHANGED]): State =>
  ({
    ...state,
    selectedPort: newPort,
  });

const startMonitoring = (state: State): State => ({
  ...state,
  ports: null,
  portFoundByConnecting: null,
});
const stopMonitoring = (state: State): State => ({
  ...state,
  ports: null,
  portFoundByConnecting: null,
});

const detectDevices = (state: State): State => ({
  ...state,
  detectingDevices: true,
  openPortErr: null,
  devices: null,
});
const devicesDetected = (state: State, { devices }: ActionsToPayloads[ActionTypes.DEVICES_DETECTED]): State => ({
  ...state,
  detectingDevices: false,
  devices,
});
const openPortErrHandler = (state: State, { openPortErr }: ActionsToPayloads[ActionTypes.OPEN_PORT_ERR]): State => ({
  ...state,
  detectingDevices: false,
  openPortErr,
});

const portOpen = (state: State): State => ({
  ...state,
  isConnected: true,
  wasForcefullyDisconnected: false,
});

const closePort = (state: State, { forceFullyDisconnected }: ActionsToPayloads[ActionTypes.CLOSE_PORT]
): State => ({
  ...state,
  isConnected: false,
  wasForcefullyDisconnected: forceFullyDisconnected,
  devices: null,
});

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.PORTS_CHANGED]: portsChanged,
  [ActionTypes.START_MONITORING_PORTS]: startMonitoring,
  [ActionTypes.STOP_MONITORING_PORTS]: stopMonitoring,
  [ActionTypes.NEW_PORT_CONNECTED]: newPortConnected,
  [ActionTypes.SELECTED_PORT_CHANGED]: selectedPortChanged,
  [ActionTypes.DETECT_DEVICES]: detectDevices,
  [ActionTypes.DEVICES_DETECTED]: devicesDetected,
  [ActionTypes.OPEN_PORT_ERR]: openPortErrHandler,
  [ActionTypes.PORT_OPEN]: portOpen,
  [ActionTypes.CLOSE_PORT]: closePort,
}, initialState);
