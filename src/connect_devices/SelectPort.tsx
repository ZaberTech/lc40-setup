import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

import { State } from '../store/reducer';
import { DotLink, Link, DotGroup, Button, Select } from '../components';
import connectImg from '../pictures/connect.png';
import { Content } from '../layout';

import {
  selectPorts, selectSelectedPort, selectPortFoundByConnecting, selectIsConnected, selectWasForcefullyDisconnected,
} from './selectors';
import { actions as actionsDefinition } from './actions';
import { Steps } from './types';

interface Props {
  actions: typeof actionsDefinition;
  ports: string[];
  selectedPort: string;
  portFoundByConnecting: string;
  isConnected: boolean;
  wasForcefullyDisconnected: boolean;
}

class SelectPortBase extends React.Component<Props> {
  componentDidMount(): void {
    const { actions } = this.props;
    actions.startMonitoringPorts();
  }

  componentWillUnmount(): void {
    const { actions } = this.props;
    actions.stopMonitoringPorts();
  }

  render(): React.ReactNode {
    const { actions, ports, selectedPort, isConnected, wasForcefullyDisconnected, portFoundByConnecting } = this.props;
    return (
      <Content className="select-port">
        <DotGroup>
          <DotLink to={Steps.SELECT_PORT} />
          <DotLink to={Steps.CONNECT_POWER} disabled={!isConnected} />
          <DotLink to={Steps.DETECT_DEVICES} disabled={!isConnected} />
        </DotGroup>

        <h3>Connect the devices to your computer</h3>
        <div className="instruction-img" style={{ backgroundImage: `url(${connectImg})` }} />

        {!wasForcefullyDisconnected && <React.Fragment>
          <p>
            Now connect the devices to your computer. The corresponding serial port will be auto-detected below.
            If the serial port is not auto-detected, you can pick it manually from the list.
          </p>
          <p>
            <b>Note:</b> If your devices are already connected, please reconnect them.
          </p>
        </React.Fragment>}

        {wasForcefullyDisconnected && <p>Connection with the device was interrupted. Reconnect the device.</p>}

        {ports && ports.length > 0 && <div className="serial-port-form">
          <h4>Your connection port:</h4>
          <Select
            placeholder={!isConnected && !selectedPort && 'Select Port'}
            options={!isConnected ? ports && ports.map(port => ({ value: port, label: port })) : []}
            value={selectedPort && { value: selectedPort, label: selectedPort }}
            isSearchable={false}
            isClearable={isConnected}
            onChange={(selectedOption, triggeredAction) => {
              if (triggeredAction && triggeredAction.action === 'clear') {
                actions.closePort(false);
              }
              if (triggeredAction && triggeredAction.action === 'select-option')
                actions.selectedPortChanged((selectedOption as { value: string }).value);
            }}
            displayClearIndicator={isConnected}
          />
          {portFoundByConnecting && <div className="port-detected">
            <FontAwesomeIcon icon={faCheckCircle}/>
            <span>&nbsp;Port detected</span>
          </div>}
        </div>}

        {ports && ports.length === 0 && <div>
          <p>Looks like your computer has no serial ports. Please connect the device.</p>
        </div>}

        <div className="button-group">
          <Link to="/physical-assembly/setup-daisy-chain"><Button className="clear" leftarrow={true}>Physical Assembly</Button></Link>
          <Button disabled={!selectedPort} className="clear" rightarrow={true}
            onClick={() => actions.navigateToStep(Steps.CONNECT_POWER)}>Next</Button>
        </div>
      </Content>);
  }
}

export const SelectPort = connect(
  (state: State): Partial<Props> => ({
    isConnected: selectIsConnected(state),
    ports: selectPorts(state),
    selectedPort: selectSelectedPort(state),
    portFoundByConnecting: selectPortFoundByConnecting(state),
    wasForcefullyDisconnected: selectWasForcefullyDisconnected(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(SelectPortBase);
