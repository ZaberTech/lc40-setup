import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import ReactTooltip from 'react-tooltip';
import classNames from 'classnames';

import { State } from '../store/reducer';
import { Button, DotLink, Link, DotGroup, Loader, QuestionIcon } from '../components';
import { Content } from '../layout';
import crossIcon from '../pictures/not_compatible.png';
import checkIcon from '../pictures/compatible.png';

import { selectDetectingDevices, selectDevices, selectCompatibleDeviceAxes, selectOpenPortErr } from './selectors';
import { actions as actionsDefinition } from './actions';
import { Device, Steps, Axis, OpenPortError, OpenPortErrorType } from './types';

interface Props {
  actions: typeof actionsDefinition;
  devices: Device[];
  detectingDevices: boolean;
  validDeviceAxes: { device: Device, axis: Axis }[];
  openPortErr: OpenPortError;
}

class DetectDevicesBase extends React.Component<Props> {
  componentDidMount(): void {
    const { actions, devices } = this.props;
    if (!devices) {
      actions.detectDevices();
    }
  }

  componentDidUpdate(previousProps: Props): void {
    if (this.props.devices !== previousProps.devices) {
      ReactTooltip.rebuild();
    }
  }

  render(): React.ReactNode {
    const { actions, devices, detectingDevices, validDeviceAxes, openPortErr } = this.props;
    const firstValidAxis = validDeviceAxes && validDeviceAxes[0];
    return (
      <Content className="detect-devices">
        <DotGroup>
          <DotLink to={Steps.SELECT_PORT} disabled={detectingDevices} />
          <DotLink to={Steps.CONNECT_POWER} disabled={detectingDevices} />
          <DotLink to={Steps.DETECT_DEVICES} />
        </DotGroup>
        <h3>Found Devices</h3>

        {!!openPortErr && <React.Fragment>
          {openPortErr.type === OpenPortErrorType.PORT_BUSY && <p>
            Cannot open the port. Make sure no other application (e.g. Zaber Console) is using the port.
          </p>}
          {openPortErr.type === OpenPortErrorType.GENERIC && <React.Fragment>
            <p>Cannot open the port.</p>
            <p>Error: {openPortErr.message}</p>
          </React.Fragment>}
        </React.Fragment>}

        {detectingDevices && <React.Fragment><p>Detecting devices... Please wait.</p><Loader /></React.Fragment>}

        {devices && devices.length === 0 && <p>No Zaber devices detected. Please connect your device and ensure that it is powered on.</p>}
        {devices && devices.length > 0 && !firstValidAxis && <p>No compatible devices detected. Please connect compatible device.</p>}

        {devices && devices.length > 0 &&
          <table className="device-list">
            <tbody>
              {devices.map(device => {
                const isCompatible = device.axes[0] && device.axes[0].isCompatible;
                return (
                  <tr key={device.deviceNumber} className={classNames({ incompatible: !isCompatible })}>
                    <td className="number">{device.deviceNumber.toString().padStart(2, '0')}</td>
                    <td className="name">{device.name}</td>
                    <td className="serial">SN: {device.serialNumber}</td>
                    <td className="compatible">
                      {isCompatible && <img className="icon cross" alt="Green checkmark" src={checkIcon} />}
                      {!isCompatible && <React.Fragment>
                        <QuestionIcon
                          data-for="compatibility-help"
                          data-tip="This device is not compatible with the LC40 Setup application" />
                        <img className="icon checkmark" alt="Gray x" src={crossIcon} />
                      </React.Fragment>
                      }
                    </td>
                  </tr>
                );
              }
              )}
            </tbody>
          </table>}

        {!detectingDevices && <div className="try-again">
          {!firstValidAxis && <Button className="clear clear-border" disabled={detectingDevices}
            onClick={actions.changePort}>
            Change Port
          </Button>}
          <Button className="clear clear-border" disabled={detectingDevices} onClick={actions.detectDevices}>
            {openPortErr ? 'Try Again' : 'Search Again'}
          </Button>
        </div>}

        <ReactTooltip className="tooltip" id="compatibility-help" />

        <div className="button-group">
          <Button disabled={detectingDevices} onClick={() => actions.navigateToStep(Steps.CONNECT_POWER)}
            className="clear" leftarrow={true}>Previous Step</Button>
          <Link
            to={
              `/config-devices/${firstValidAxis && firstValidAxis.device.deviceNumber}/${firstValidAxis && firstValidAxis.axis.axisNumber}`
            }>
            <Button disabled={detectingDevices || !firstValidAxis}
              className="clear" rightarrow={true}>Device Configuration</Button>
          </Link>
        </div>
      </Content>
    );
  }
}

export const DetectDevices = connect(
  (state: State): Partial<Props> => ({
    devices: selectDevices(state),
    detectingDevices: selectDetectingDevices(state),
    validDeviceAxes: selectCompatibleDeviceAxes(state),
    openPortErr: selectOpenPortErr(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(DetectDevicesBase);
