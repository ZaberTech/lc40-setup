import React from 'react';
import { Switch, Route } from 'react-router-dom';

import { DetectDevices } from './DetectDevices';
import { ConnectPower } from './ConnectPower';
import { SelectPort } from './SelectPort';
import { Steps } from './types';

export const ConnectDevices: React.FC = () => (
  <Switch>
    <Route path={Steps.DETECT_DEVICES} component={DetectDevices} />
    <Route path={Steps.CONNECT_POWER} component={ConnectPower} />
    <Route path={Steps.SELECT_PORT} component={SelectPort} />
  </Switch>
);
