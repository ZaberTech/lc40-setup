import * as router from 'connected-react-router';

import { actionBuilder } from '../utils';

import { Steps, Device, OpenPortError } from './types';

export const enum ActionTypes {
  START_MONITORING_PORTS = 'START_MONITORING_PORTS',
  STOP_MONITORING_PORTS = 'STOP_MONITORING_PORTS',
  PORTS_CHANGED = 'PORTS_CHANGED',
  NEW_PORT_CONNECTED = 'NEW_PORT_CONNECTED',
  SELECTED_PORT_CHANGED = 'SELECTED_PORT_CHANGED',
  DETECT_DEVICES = 'DETECT_DEVICES',
  DEVICES_DETECTED = 'DEVICES_DETECTED',
  OPEN_PORT_ERR = 'OPEN_PORT_ERR',
  PORT_OPEN = 'PORT_OPEN',
  CLOSE_PORT = 'CLOSE_PORT',
  CHANGE_PORT = 'CHANGE_PORT',
}

export interface ActionsToPayloads {
  [ActionTypes.PORTS_CHANGED]: { ports: string[] };
  [ActionTypes.START_MONITORING_PORTS]: void;
  [ActionTypes.STOP_MONITORING_PORTS]: void;
  [ActionTypes.CLOSE_PORT]: { forceFullyDisconnected: boolean };
  [ActionTypes.NEW_PORT_CONNECTED]: { port: string };
  [ActionTypes.SELECTED_PORT_CHANGED]: { newPort: string };
  [ActionTypes.DETECT_DEVICES]: void;
  [ActionTypes.DEVICES_DETECTED]: { devices: Device[] };
  [ActionTypes.OPEN_PORT_ERR]: { openPortErr: OpenPortError };
  [ActionTypes.PORT_OPEN]: void;
  [ActionTypes.CHANGE_PORT]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  portsChanged: (ports: string[]) => buildAction(ActionTypes.PORTS_CHANGED, { ports }),
  startMonitoringPorts: () => buildAction(ActionTypes.START_MONITORING_PORTS),
  stopMonitoringPorts: () => buildAction(ActionTypes.STOP_MONITORING_PORTS),
  closePort: (forceFullyDisconnected: boolean) => buildAction(ActionTypes.CLOSE_PORT, { forceFullyDisconnected }),
  newPortConnected: (port: string) => buildAction(ActionTypes.NEW_PORT_CONNECTED, { port }),
  selectedPortChanged: (newPort: string) => buildAction(ActionTypes.SELECTED_PORT_CHANGED, { newPort }),
  navigateToStep: (step: Steps) => router.replace(step),
  detectDevices: () => buildAction(ActionTypes.DETECT_DEVICES),
  devicesDetected: (devices: Device[]) => buildAction(ActionTypes.DEVICES_DETECTED, { devices }),
  openPortErr: (openPortErr: OpenPortError) => buildAction(ActionTypes.OPEN_PORT_ERR, { openPortErr }),
  portOpen: () => buildAction(ActionTypes.PORT_OPEN),
  changePort: () => buildAction(ActionTypes.CHANGE_PORT),
};
