import React from 'react';

import { DotLink, Link, DotGroup, Button } from '../components';
import { Content } from '../layout';
import powerImg from '../pictures/power.png';

import { Steps } from './types';

export class ConnectPower extends React.Component {
  public render(): React.ReactNode {
    return (
      <Content className="detect-devices">
        <DotGroup>
          <DotLink to={Steps.SELECT_PORT} />
          <DotLink to={Steps.CONNECT_POWER} />
          <DotLink to={Steps.DETECT_DEVICES} />
        </DotGroup>
        <h3>Connect the power supply</h3>
        <div className="instruction-img" style={{ backgroundImage: `url(${powerImg})` }} />
        <p>
          Plug the included power supply into the device, then into the wall.
        </p>
        <p>
          If you have multiple devices in a daisy-chain, connect a power supply to every device.
        </p>
        <div className="button-group">
          <Link to={Steps.SELECT_PORT}><Button className="clear" leftarrow={true}>Previous Step</Button></Link>
          <Link to={Steps.DETECT_DEVICES}><Button className="clear" rightarrow={true}>Next Step</Button></Link>
        </div>
      </Content>
    );
  }
}
