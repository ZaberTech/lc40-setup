import React from 'react';
import { injectable, Container } from 'inversify';
import { render, RenderResult } from '@testing-library/react';
import { waitFor, fireEvent } from '@testing-library/dom';
import { ReplaySubject } from 'rxjs';

import '../test/mocks/Select';
import { wrapWithNewStore, wrapWithRouter } from '../test';
import { createContainer, destroyContainer } from '../container';
import { ZML } from '../zml';

import { SelectPort } from './SelectPort';
import { actions } from './actions';

const TestSelectPort = wrapWithNewStore(wrapWithRouter(SelectPort));

jest.useFakeTimers();

let ports: string[];

class ConnectionMock {
  public disconnected = new ReplaySubject();
}

let zmlMock: ZmlMock;

@injectable()
class ZmlMock {
  public connection = new ConnectionMock();
  public close = jest.fn(() => Promise.resolve());

  public async listPorts(): Promise<string[]> {
    return ports;
  }

  constructor() {
    zmlMock = this;
  }
}

let container: Container;

beforeAll(() => {
  container = createContainer();
  container.bind<unknown>(ZML).to(ZmlMock);
});

afterAll(() => {
  destroyContainer();
  zmlMock = null;
});

let wrapper: RenderResult;

beforeEach(() => {
  ports = ['COM5'];

  wrapper = render(<TestSelectPort />);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null;
});

test('scans for ports and offers them to the user', async () => {
  wrapper.getByText('Connect the devices to your computer');

  expect(wrapper.getAllByTestId('select-mock-option').map(option => option.textContent)).toStrictEqual(['COM5']);
});

test('detects newly connected port and offers it to the user', async () => {
  expect(wrapper.getAllByTestId('select-mock-option')).toHaveLength(1);

  ports = ['COM5', 'COM6'];
  jest.advanceTimersByTime(1000);

  await waitFor(() =>
    expect(wrapper.getAllByTestId('select-mock-option').map(option => option.textContent)).toStrictEqual(['COM5', 'COM6'])
  );

  expect(wrapper.getByTestId('select-mock').getAttribute('data-value')).toBe('COM6');
});

test('removes port if it disappears', async () => {
  expect(wrapper.getAllByTestId('select-mock-option')).toHaveLength(1);

  ports = ['COM6'];
  jest.advanceTimersByTime(1000);

  await waitFor(() =>
    expect(wrapper.getAllByTestId('select-mock-option').map(option => option.textContent)).toStrictEqual(['COM6'])
  );
});

test('changes the port', async () => {
  ports = ['COM6', 'COM7'];
  jest.advanceTimersByTime(1000);

  await waitFor(() =>
    expect(wrapper.getAllByTestId('select-mock-option')).toHaveLength(2)
  );

  fireEvent.change(wrapper.getByTestId('select-mock'), { target: { value: 'COM7' } });

  expect(wrapper.getByTestId('select-mock').getAttribute('data-value')).toBe('COM7');
});

describe('when connected', () => {
  beforeEach(() => {
    TestSelectPort.testStore.dispatch(actions.selectedPortChanged('COM5'));
    TestSelectPort.testStore.dispatch(actions.portOpen());
  });

  test('displays disconnect button; disconnects upon clicking', async () => {
    expect(wrapper.queryByTestId('select-mock-clear')).toBeDefined();
    expect(wrapper.queryAllByTestId('select-mock-option')).toHaveLength(0);
    expect(wrapper.getByTestId('select-mock').getAttribute('data-value')).toBe('COM5');

    fireEvent.click(wrapper.getByTestId('select-mock-clear'));

    expect(zmlMock.close).toHaveBeenCalledTimes(1);
    expect(wrapper.queryByTestId('select-mock-clear')).toBeNull();
    expect(wrapper.queryAllByTestId('select-mock-option')).toHaveLength(1);
  });

  test('displays message when port was forcefully disconnected', async () => {
    TestSelectPort.testStore.dispatch(actions.closePort(true));

    wrapper.getByText(/Connection with the device was interrupted. Reconnect the device./);
  });
});

