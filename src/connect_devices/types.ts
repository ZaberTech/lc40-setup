export enum Steps {
  SELECT_PORT = '/connect-devices/select-port',
  DETECT_DEVICES = '/connect-devices/detect-devices',
  CONNECT_POWER = '/connect-devices/connect-power',
}

export interface OpenPortError {
  type: OpenPortErrorType;
  message: string;
}

export enum OpenPortErrorType {
  GENERIC = 'GENERIC',
  PORT_BUSY = 'PORT_BUSY',
}

export interface Axis {
  axisNumber: number;
  isCompatible: boolean;

  isPeripheral: boolean;
  peripheralId: number;
  peripheralName: string;
}

export interface Device {
  deviceId: number;
  deviceNumber: number;
  name: string;
  serialNumber: number;

  isController: boolean;
  axes: Axis[];
}

export interface DeviceAxisPair {
  device: Device;
  axis: Axis;
}
