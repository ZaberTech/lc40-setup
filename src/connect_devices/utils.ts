import { ascii } from '@zaber/motion';
import _ from 'lodash';

import { environment } from '../environment';

import { Device, Axis } from './types';

export const COMPATIBLE_DEVICE_IDS = [50911, !environment.isProduction && 43211];

export function zmlDevicesToDevices(devices: ascii.Device[]): Device[] {
  return devices.map(device => {
    const info = device.identity;

    const axes: Axis[] = _.range(1, info.axisCount + 1).map(axisNumber => {
      const axisInfo = device.getAxis(axisNumber).identity;
      const isCompatible = COMPATIBLE_DEVICE_IDS.includes(info.deviceId) || COMPATIBLE_DEVICE_IDS.includes(axisInfo.peripheralId);
      return ({
        axisNumber,
        isCompatible,
        isPeripheral: axisInfo.isPeripheral,
        peripheralId: axisInfo.peripheralId,
        peripheralName: axisInfo.peripheralName,
      });
    });

    const appDevice: Device = ({
      deviceNumber: device.deviceAddress,
      name: info.name,
      serialNumber: info.serialNumber,
      deviceId: info.deviceId,
      axes,
      isController: axes.some(axis => axis.isPeripheral),
    });

    return appDevice;
  });
}