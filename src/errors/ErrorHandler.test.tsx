import React from 'react';
import { render, RenderResult } from '@testing-library/react';

import { ErrorHandler } from './ErrorHandler';
import { errorEmitter } from './errors';

let wrapper: RenderResult;

const TestComponent = () => (
  <ErrorHandler>
    <div>Ordinary Content</div>
  </ErrorHandler>
);

beforeEach(() => {
  wrapper = render(<TestComponent/>);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null;
});

test('renders ordinary content without error', () => {
  wrapper.getAllByText(/Ordinary Content/);
});

test('renders error report when error is emitted', () => {
  errorEmitter.emit('error');

  expect(wrapper.queryByText(/Ordinary Content/)).toBeNull();
  expect(wrapper.getByText(/Zaber Support/)).toBeDefined();
});
