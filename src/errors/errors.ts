import * as Sentry from '@sentry/react';
import { EventEmitter } from 'events';

import { environment } from '../environment';

export const errorEmitter = new EventEmitter();

function beforeSend(event: Sentry.Event): Sentry.Event {
  if (typeof event.level === 'string' && event.level.match(/^(error|fatal|critical)$/)) {
    process.nextTick(() => errorEmitter.emit('error'));
  }
  return event;
}

export function initErrors(): void {
  const dsn = process.env.SENTRY_DSN;
  if (environment.isProduction && !dsn) {
    throw new Error(`Sentry.io token missing: ${dsn}`);
  }

  Sentry.init({
    dsn,
    enabled: environment.isProduction,
    release: environment.releaseName,
    beforeSend,
  });
}

export function handleError(err: unknown): void {
  Sentry.captureException(err);
}
