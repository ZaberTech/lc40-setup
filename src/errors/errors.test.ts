const sentryMock = {
  init: jest.fn(),
  captureException: jest.fn(),
};

function sentryMockRequire(): unknown {
  return sentryMock;
}

jest.mock('@sentry/react', sentryMockRequire);

// eslint-disable-next-line
import { initErrors, errorEmitter, handleError } from './errors';

beforeEach(() => {
  sentryMock.init.mockReset();
  sentryMock.captureException.mockReset();
});

afterEach(() => {
  errorEmitter.removeAllListeners('error');
});

test('handleError calls captureException', () => {
  const error = new Error('Ups!');
  handleError(error);

  expect(sentryMock.captureException).toBeCalledWith(error);
});

test('sending event with level error emits error', async () => {
  initErrors();

  expect(sentryMock.init).toBeCalledTimes(1);
  const initArg = sentryMock.init.mock.calls[0][0];

  const emitterPromise = new Promise(resolve => errorEmitter.once('error', resolve));

  initArg.beforeSend({
    level: 'error',
  });

  await emitterPromise;
});
