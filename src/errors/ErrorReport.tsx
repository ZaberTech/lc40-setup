import React from 'react';

export const ErrorReport: React.FunctionComponent = () => (
  <div>
    <p>We have encountered an error. Sorry!</p>
    <p>The error was automatically reported.</p>
    <p>Please restart the app.</p>
    <p>
      In case of persisting issue,
      please contact Zaber Support at <a href="https://www.zaber.com/contact">https://www.zaber.com/contact</a>.
    </p>
  </div>
);
