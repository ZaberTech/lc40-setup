import React from 'react';
import { connect } from 'react-redux';
import { Dispatch, bindActionCreators } from 'redux';

import { Checkbox, Link, Button } from '../components';
import splashScreenImg from '../pictures/welcome_picture_cropped.png';
import circleOneImg from '../pictures/step_number1.png';
import circleTwoImg from '../pictures/step_number2.png';
import circleThreeImg from '../pictures/step_number3.png';
import { State } from '../store/reducer';
import { PHYSICAL_ASSEMBLY_PATH } from '../physical_assembly';

import { selectShowSplashScreen } from './selectors';
import { actions as actionsDefinition } from './actions';

const ArrowRight = () =>
  <svg className="left" id="bg" height="100%" width="15" viewBox="0 0 50 100" preserveAspectRatio="none">
    <path d="M0,0 h0 l45,50 l-45,50 h0z" />
  </svg>;

const ArrowLeft = () =>
  <svg className="right" id="bg" height="100%" width="15" viewBox="0 0 50 100" preserveAspectRatio="none">
    <path d="M 50 0 L 50 100 L 0 100 L 50 50 L 0 0 L 50 0" />
  </svg>;

interface Props {
  actions: typeof actionsDefinition;
  showSplashScreen: boolean;
}

const SplashScreenBase: React.FunctionComponent<Props> = ({ showSplashScreen, actions }) => (
  <div className="splashscreen">
    <div className="img-container" style={{ backgroundImage: `url(${splashScreenImg})` }} />
    <div className="title">Set up your LC40 with our guide</div>
    <div className="content">
      <div className="arrow-container">
        <div className="arrow">
          <img src={circleOneImg} className="number-icon" alt="number one with a circle around it" />
          <ArrowRight />
          Physical Assembly
        </div>
        <div className="arrow">
          <img src={circleTwoImg} className="number-icon" alt="number two with a circle around it" />
          <ArrowLeft />
          <ArrowRight />
          Device Connection
        </div>
        <div className="arrow">
          <img src={circleThreeImg} className="number-icon" alt="number three with a circle around it" />
          <ArrowLeft />
          <ArrowRight />
          Device Configuration
        </div>
        {/*<div className="arrow">
          <img src={circleFourImg} className="number-icon" alt="number four with a circle around it" />
          <ArrowLeft />
          <ArrowRight />
          Device Tuning
        </div>*/}
      </div>
      <Link to={PHYSICAL_ASSEMBLY_PATH}>
        <Button>Start</Button>
      </Link>
      <p className="do-not-show">
        <Checkbox checked={!showSplashScreen} onChange={checked => actions.showSplashChanged(!checked)}>
          Do not show the Welcome Page again
        </Checkbox>
      </p>
    </div>
  </div>
);


export const SplashScreen = connect(
  (state: State): Partial<Props> => ({
    showSplashScreen: selectShowSplashScreen(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(SplashScreenBase);
