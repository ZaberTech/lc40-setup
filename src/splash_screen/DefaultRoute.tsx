import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

import { State } from '../store/reducer';
import { PHYSICAL_ASSEMBLY_PATH } from '../physical_assembly';

import { selectShowSplashScreen } from './selectors';

interface Props {
  showSplashScreen: boolean;
}

const DefaultRouteBase: React.FunctionComponent<Props> = ({ showSplashScreen }) =>
  (<Route><Redirect to={showSplashScreen ? '/splash' : PHYSICAL_ASSEMBLY_PATH} /></Route>);

export const DefaultRoute = connect(
  (state: State): Partial<Props> => ({
    showSplashScreen: selectShowSplashScreen(state),
  }),
)(DefaultRouteBase);
