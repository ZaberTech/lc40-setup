import { all, takeLatest, fork, put } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import { Action } from '../utils';
import { Storage, StorageKeys } from '../storage';
import { getContainer } from '../container';

import { ActionTypes, ActionsToPayloads, actions } from './actions';
import { SHOW_SPLASH_SCREEN_DEFAULT } from './types';

export function* splashScreenSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.SHOW_SPLASH_CHANGED, showSplashChanged),
    fork(loadShowSplashScreen),
  ]);
}

// eslint-disable-next-line require-yield
function* showSplashChanged(action: Action<ActionsToPayloads[ActionTypes.SHOW_SPLASH_CHANGED]>): SagaIterator {
  const storage = getContainer().get<Storage>(Storage);
  storage.save(StorageKeys.SHOW_SPLASH_SCREEN, action.value.show);
}

function* loadShowSplashScreen(): SagaIterator {
  const storage = getContainer().get<Storage>(Storage);
  const showSplashScreen = storage.load(StorageKeys.SHOW_SPLASH_SCREEN, SHOW_SPLASH_SCREEN_DEFAULT);
  yield put(actions.showSplashLoaded(showSplashScreen));
}
