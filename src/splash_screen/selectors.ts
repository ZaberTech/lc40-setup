import { createSelector } from 'reselect';

import { selectSplashScreen } from '../store';

export const selectShowSplashScreen = createSelector(selectSplashScreen, state => state.showSplashScreen);
