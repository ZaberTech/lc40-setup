import React from 'react';
import { Container, injectable } from 'inversify';
import { render, RenderResult, fireEvent } from '@testing-library/react';

import { wrapWithNewStore, wrapWithRouter } from '../test';
import { createContainer, destroyContainer } from '../container';
import { buildStore } from '../store/build';
import { Storage, StorageKeys } from '../storage';

import { SplashScreen } from './SplashScreen';
import { SHOW_SPLASH_SCREEN_DEFAULT } from './types';

const TestSplashScreen = wrapWithNewStore(wrapWithRouter(SplashScreen));

let container: Container;
let storageMock: StorageMock;

@injectable()
class StorageMock {
  public load = jest.fn(() => true);
  public save = jest.fn();
}

let wrapper: RenderResult;
let store: ReturnType<typeof buildStore>;

beforeEach(() => {
  container = createContainer();
  container.bind<unknown>(Storage).to(StorageMock);
  storageMock = (container.get(Storage) as unknown) as StorageMock;

  store = buildStore();

  wrapper = render(<TestSplashScreen store={store} />);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null;

  destroyContainer();
});

test('loads show splash screen from the storage', () => {
  expect(storageMock.load).toHaveBeenCalledWith(StorageKeys.SHOW_SPLASH_SCREEN, SHOW_SPLASH_SCREEN_DEFAULT);

  expect(wrapper.getByRole('checkbox')).not.toHaveAttribute('checked');
});

test('saves show splash screen to the storage when changed', async () => {
  expect(storageMock.save).not.toHaveBeenCalled();

  wrapper.getByRole('checkbox').setAttribute('checked', '');
  fireEvent.click(wrapper.getByRole('checkbox'));

  expect(storageMock.save).toHaveBeenCalledWith(StorageKeys.SHOW_SPLASH_SCREEN, false);
});
