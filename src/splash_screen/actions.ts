import { actionBuilder } from '../utils';

export const enum ActionTypes {
  SHOW_SPLASH_CHANGED = 'SHOW_SPLASH_CHANGED',
  SHOW_SPLASH_LOADED = 'SHOW_SPLASH_LOADED',
}

export interface ActionsToPayloads {
  [ActionTypes.SHOW_SPLASH_CHANGED]: { show: boolean };
  [ActionTypes.SHOW_SPLASH_LOADED]: { show: boolean };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  showSplashChanged: (show: boolean) => buildAction(ActionTypes.SHOW_SPLASH_CHANGED, { show }),
  showSplashLoaded: (show: boolean) => buildAction(ActionTypes.SHOW_SPLASH_LOADED, { show }),
};
