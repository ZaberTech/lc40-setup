export { SplashScreen } from './SplashScreen';
export { State as SplashScreenState, reducer as splashScreenReducer } from './reducer';
export {
  actions as splashScreenActions,
  ActionsToPayloads as SplashScreenActionPayloads,
  ActionTypes as SplashScreenActionTypes,
} from './actions';
export { splashScreenSaga } from './sagas';
export { DefaultRoute } from './DefaultRoute';
