import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';

export interface State {
  showSplashScreen: boolean;
}

const initialState: State = {
  showSplashScreen: null,
};

const showSplashChangedOrLoaded = (state: State, { show }: ActionsToPayloads[ActionTypes.SHOW_SPLASH_CHANGED]): State =>
  ({
    ...state,
    showSplashScreen: show,
  });

export const reducer = createReducer<ActionsToPayloads, typeof initialState>({
  [ActionTypes.SHOW_SPLASH_CHANGED]: showSplashChangedOrLoaded,
  [ActionTypes.SHOW_SPLASH_LOADED]: showSplashChangedOrLoaded,
}, initialState);
