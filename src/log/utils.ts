import { getContainer } from '../container';

import { Logger } from './logger';
import { Log } from './log';

export function getLogger(name: string): Logger {
  return getContainer().get(Log).getLogger(name);
}
