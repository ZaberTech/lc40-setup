import { injectable } from 'inversify';

import { environment } from '../environment';

import { Logger } from './logger';
import { LogLevel } from './types';

const logFunctions = {
  [LogLevel.Info]: console.log, // tslint:disable-line:no-console
  [LogLevel.Warning]: console.warn, // tslint:disable-line:no-console
  [LogLevel.Error]: console.error, // tslint:disable-line:no-console
};

@injectable()
export class Log {
  private readonly loggers = new Map<string, Logger>();

  public getLogger(name: string): Logger {
    let logger = this.loggers.get(name);
    if (!logger) {
      logger = new Logger(this, name);
      this.loggers.set(name, logger);
    }
    return logger;
  }

  public log(level: LogLevel, name: string, ...args: unknown[]): void {
    if (environment.isTest) {
      return;
    }

    const message = `${new Date().toISOString()} ${level} ${name}:`;
    logFunctions[level](message, ...args);
  }
}
