import React from 'react';

export interface Props {
  children: React.ReactNode;
}

export const DotGroup = ({ children }: Props) => (
  <ul className="dot group">
    {children}
  </ul>
);
