import React from 'react';
import { shell } from 'electron';

interface Props {
  url: string;
  children: React.ReactNode;
}

export const ExternalLink: React.FunctionComponent<Props> = ({ children, url }) =>
  <a className="external-link" onClick={() => shell.openExternal(url)}>{children}</a>;
