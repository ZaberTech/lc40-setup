
import React from 'react';
import { useRouteMatch } from 'react-router-dom';
import classNames from 'classnames';

import { Link, Props } from './Link';

export const SidebarLink = (props: Props) => {
  const desiredLinkPath = `/${props.to.toString().split('/')[1]}`;
  const match = useRouteMatch(desiredLinkPath);
  return (
    <React.Fragment>
      {match && <div className="layout menu selected" />}
      <Link className={classNames({ bold: match })} {...props}>{props.children}</Link>
      {match &&
        <svg id="bg" height="100%" width="20" viewBox="0 0 50 100" preserveAspectRatio="none">
          <path d="M0,0 h0 l45,50 l-45,50 h0z" />
        </svg>}
    </React.Fragment>
  );
};
