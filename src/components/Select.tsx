import React from 'react';
import { default as OriginalSelect, Theme } from 'react-select';
import { components, IndicatorProps, Props as OriginalSelectProps } from 'react-select';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faTimesCircle } from '@fortawesome/free-solid-svg-icons';

const CustomClearIndicator = (props: IndicatorProps<{ label: string; value: string; }>) => {
  return (
    <components.ClearIndicator {...props}>
      <FontAwesomeIcon icon={faTimesCircle} className="clear-icon" />
    </components.ClearIndicator>
  );
};

interface Props extends OriginalSelectProps {
  displayClearIndicator?: boolean;
}

const selectBoxTheme = (theme: Theme): Theme => ({
  ...theme,
  colors: {
    ...theme.colors,
    primary25: '#DDDDDD',
    primary50: '#ffffff',
    primary: '#888888',
  },
});

export const Select = (props: Props) => {
  return (
    <OriginalSelect
      {...props}
      theme={selectBoxTheme}
      classNamePrefix="select"
      components={props.displayClearIndicator && {
        DropdownIndicator: () => null,
        IndicatorSeparator: () => null,
        Menu: () => null,
        ClearIndicator: CustomClearIndicator,
      }}
    />
  );
};