import React, { useState } from 'react';
import classNames from 'classnames';

import questionIcon from '../pictures/help_scaled.png';
import questionIconHover from '../pictures/help_hover.png';

export interface Props extends React.HTMLProps<HTMLImageElement> {
  crossOrigin?: '' | 'anonymous' | 'use-credentials';
}

export const QuestionIcon = (props: Props) => {
  const { className, ...imageProps } = props;
  const [viewHoverIcon, setViewHoverIcon] = useState(false);
  return (
    <img
      { ...imageProps }
      alt="Gray question mark"
      className={classNames({ icon: true, question: true }, className)}
      onMouseOver={() => setViewHoverIcon(true)}
      onMouseOut={() => setViewHoverIcon(false)}
      src={viewHoverIcon ? questionIconHover : questionIcon} />
  );
};