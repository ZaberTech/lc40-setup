import React from 'react';
import classNames from 'classnames';

import arrowImg from '../pictures/arrow.svg';

export interface Props extends React.HTMLAttributes<HTMLButtonElement> {
  leftarrow?: boolean;
  rightarrow?: boolean;
  children?: React.ReactNode;
  disabled?: boolean;
}

export const Button = (props: Props) => {
  const { leftarrow, rightarrow, children, className, ...buttonprops } = props;
  return (
    <button {...buttonprops} className={classNames({ button: true, disabled: props.disabled }, className)}>
      {leftarrow && <img className="icon arrow-left" alt="Gray arrow facing right" src={arrowImg} />}
      {children}
      {rightarrow && <img className="icon arrow-right" alt="Gray arrow facing left" src={arrowImg} />}
    </button>
  );
};