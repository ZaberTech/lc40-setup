import React from 'react';
import classNames from 'classnames';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheckCircle } from '@fortawesome/free-solid-svg-icons';

import { ReactComponent as CrossIcon } from '../pictures/cross.svg';

import { Button } from './Button';

interface Props {
  title: string;
  children: React.ReactNode;
  type: 'error' | 'success';
  onRequestClose: () => void;
  isOpen: boolean;
}

class Modal extends React.Component<Props> {
  render(): JSX.Element {
    const { title, children, type, onRequestClose, isOpen } = this.props;
    return (
      <div className="modal" style={{ display: !isOpen && 'none' }}>
        <div className={
          classNames({
            'modal-header': true,
            error: type === 'error',
            success: type === 'success'
          })}>
          <div className="icon">
            {type === 'error' && <CrossIcon className="error-cross" />}
            {type === 'success' && <FontAwesomeIcon icon={faCheckCircle} />}
          </div>
          <h2>{title}</h2>
        </div>
        <div className="content">
          {children}
          <div className="button-group">
            <Button onClick={() => onRequestClose()}>Ok</Button>
          </div>
        </div>
      </div>
    );
  }
}

export { Modal };
