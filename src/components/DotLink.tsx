import React from 'react';
import { NavLink, NavLinkProps } from 'react-router-dom';

import { Link } from './Link';
export interface Props extends NavLinkProps {
  disabled?: boolean;
}

export const DotLink = (props: Props) => {
  const { disabled, ...linkprops } = props;
  return (
    <li className="dot">
      {!disabled ? <NavLink activeClassName="selected" {...linkprops} /> : <Link {...linkprops} disabled={true} />}
    </li>
  );
};