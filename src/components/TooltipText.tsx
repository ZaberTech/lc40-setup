import React from 'react';
import classNames from 'classnames';

type Props = React.DetailedHTMLProps<React.HTMLAttributes<HTMLSpanElement>, HTMLSpanElement>;

export const TooltipText: React.FC<Props> = ({ className, ...rest }) =>
  <span className={classNames('text-tooltip', className)} {...rest}/>;
