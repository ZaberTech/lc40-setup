import React from 'react';
import { remote } from 'electron';

import closeIcon from '../pictures/system_close.svg';
import minimizeIcon from '../pictures/system_minimize.svg';

const isWindows = process.platform === 'win32';

const minimize = () => remote.BrowserWindow.getAllWindows()[0].minimize();
const close = () => remote.BrowserWindow.getAllWindows()[0].close();

function Header(props: { children: React.ReactNode }): JSX.Element {
  if (!isWindows) {
    return null;
  }
  return (
    <div className="header">
      <div className="content">
        {props.children}
      </div>
      <div className="header-icons">
        <div className="system-button" onClick={minimize}>
          <img src={minimizeIcon} alt="Minimize"/>
        </div>
        <div className="system-button" onClick={close}>
          <img src={closeIcon} alt="Close"/>
        </div>
      </div>
    </div>
  );
}

export { Header };
