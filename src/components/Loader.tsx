import React from 'react';
import classNames from 'classnames';

import loaderSvg from '../pictures/loader.svg';

interface Props {
  show?: boolean;
}

export const Loader: React.FC<Props> = ({ show }) => (
  <img
    className={classNames({ hidden: typeof show === 'boolean' ? !show : false }, 'loader')}
    src={loaderSvg}
    alt="Spinning Gear Loading Animation"/>
);
