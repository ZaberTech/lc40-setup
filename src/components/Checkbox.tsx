import React from 'react';
import CheckboxLib, { Props as CheckboxLibProps } from 'rc-checkbox';

interface Props extends Omit<CheckboxLibProps, 'onChange'> {
  children: React.ReactNode;
  onChange: (checked: boolean) => void;
}

export const Checkbox = (props: Props) => {
  const { children, onChange, ...restOfProps } = props;
  return (
    <label>
      <CheckboxLib {...restOfProps} onChange={(e: unknown) => onChange((e as { target: { checked: boolean } }).target.checked)} />
      &nbsp; {children}
    </label>
  );
};
