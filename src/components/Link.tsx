
import React from 'react';
import classNames from 'classnames';
import { Link as LinkOriginal, LinkProps } from 'react-router-dom';

export interface Props extends LinkProps {
  disabled?: boolean;
}

export const Link = (props: Props) => {
  const { disabled, ...originalLinkProps } = props;
  if (props.disabled) {
    return <span className={classNames('disabled', originalLinkProps.className)}>{props.children}</span>;
  }
  return <LinkOriginal {...originalLinkProps}/>;
};
