import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { IconProp } from '@fortawesome/fontawesome-svg-core';
import classNames from 'classnames';

import { Button, Props as ButtonProps } from './Button';

interface Props extends ButtonProps {
  icon: IconProp;
}

export const ButtonWithIcon = (props: Props) => {
  const { icon, children, className, ...buttonprops } = props;
  return (
    <Button className={classNames('button-with-icon', className)} {...buttonprops}>
      <span className="children-span">{children}</span>
      <FontAwesomeIcon icon={icon} />
    </Button>
  );
};
