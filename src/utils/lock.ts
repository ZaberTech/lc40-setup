import { Mutex, MutexInterface } from 'async-mutex';
import { CANCEL } from 'redux-saga';

interface Unlock {
  unlock: MutexInterface.Releaser;
}

interface SagaCancellablePromise {
  [cancel: string]: () => void;
}

export class Lock {
  private readonly _lock = new Mutex();

  constructor() {
    this.lock = this.lock.bind(this);
  }

  public lock(): Promise<Unlock> {
    const lockPromise = this._lock.acquire().then(unlock => ({ unlock }));

    // this will unlock the lock if the call effect is cancelled
    (lockPromise as unknown as SagaCancellablePromise)[CANCEL] = () => lockPromise.then(({ unlock }) => unlock());

    return lockPromise;
  }
}
