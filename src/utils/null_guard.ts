/* eslint-disable prefer-rest-params, @typescript-eslint/no-explicit-any */
export function nullGuard<T extends (...args: any[]) => unknown>(guardedFunction: T): T {
  return function(this: unknown): unknown {
    for (let i = 0; i < arguments.length; i++) { // tslint:disable-line:prefer-for-of
      if (arguments[i] === null) {
        return null;
      }
    }
    return guardedFunction.apply(this, arguments as any); // eslint-disable-line
  } as T;
}
