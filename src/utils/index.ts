import { AnyAction } from 'redux';

export { nullGuard } from './null_guard';
export { Lock } from './lock';

export const actionBuilder = <T>(type: string, value?: T) => ({ type, value });
export const getPayload = <T>(action: Record<string, unknown>) => (action.value || action.payload) as T;

type ActionToReducerMap<Map, State> = Partial<{ [Key in keyof Map]: (state: State, value: Map[Key]) => State }>;

export const createReducer = <Map, State>(map: ActionToReducerMap<Map, State>, initialState: State) => (
  (state: State = initialState, action: AnyAction) => {
    const reducer = map[action.type as keyof Map];
    if (reducer) {
      return reducer(state, getPayload(action));
    } else {
      return state;
    }
  }
);

export type RecursivePartial<T> = {
  [P in keyof T]?: RecursivePartial<T[P]>;
};

export type Action<T> = { type: string; value: T; };
