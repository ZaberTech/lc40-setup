import { injectable } from 'inversify';
import { Tools, ascii, Library, DeviceDbSourceType, LogOutputMode } from '@zaber/motion';

import { environment } from '../environment';

@injectable()
export class ZML {
  private _connection: ascii.Connection = null;
  private _connecting = false;

  public get connection(): ascii.Connection {
    return this._connection;
  }

  constructor() {
    if (!environment.isProduction) {
      Library.setLogOutput(LogOutputMode.STDERR);
    }
    if (!environment.isRelease) {
      Library.setDeviceDbSource(DeviceDbSourceType.WEB_SERVICE, 'https://api.zaber.io/device-db/master');
    }
  }

  public listPorts(): Promise<string[]> {
    return Tools.listSerialPorts();
  }

  public async openSerialPort(serialPort: string): Promise<void> {
    if (this._connection || this._connecting) {
      throw new Error('Connection is already opened');
    }

    this._connecting = true;
    try {
      const tcpMach = serialPort.match(/^tcp:\/\/([^:]+):(\d+)$/);
      if (tcpMach) {
        this._connection = await ascii.Connection.openTcp(tcpMach[1], +tcpMach[2]);
      } else {
        this._connection = await ascii.Connection.openSerialPort(serialPort);
      }
    } finally {
      this._connecting = false;
    }
  }

  public async close(): Promise<void> {
    const connection = this._connection;
    this._connection = null;

    if (connection) {
      await connection.close();
    }
  }
}