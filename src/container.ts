import { Container } from 'inversify';

import { LocalStorageSymbol } from './storage';

let container: Container;

export function getContainer(): Container {
  return container;
}

export function createContainer(): Container {
  container = new Container({ autoBindInjectable: true, defaultScope: 'Singleton' });

  container.bind(LocalStorageSymbol).toConstantValue(window.localStorage);

  return container;
}

export function destroyContainer(): void {
  if (container) {
    container.unbindAll();
    container = null;
  }
}
