import 'reflect-metadata';
import React from 'react';
import ReactDOM from 'react-dom';

import './styles/index.scss';
import './styles/bootstrap.css';
import App from './App';
import { createContainer } from './container';
import { buildStore } from './store/build';
import { initErrors } from './errors';

initErrors();
createContainer();
const store = buildStore();

ReactDOM.render(<App store={store}/>, document.getElementById('root'));
