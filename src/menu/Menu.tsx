import React from 'react';
import { connect } from 'react-redux';

import { State } from '../store/reducer';
import { selectFirstDevice } from '../config_devices/selectors';
import { DeviceAxisPair } from '../connect_devices';
import { SidebarLink } from '../components';

import { selectCanNavigate, selectCanNavigateAndHasDevices } from './selectors';

interface Props {
  canNavigate: boolean;
  canNavigateToDevices: boolean;
  firstDevice: DeviceAxisPair;
}

class MenuBase extends React.Component<Props> {
  render(): React.ReactNode {
    const { canNavigate, canNavigateToDevices, firstDevice } = this.props;
    return (
      <ul>
        <li>
          <SidebarLink disabled={!canNavigate} to="/physical-assembly/motor-coupling">Physical Assembly</SidebarLink>
        </li>
        <li>
          <SidebarLink disabled={!canNavigate} to="/connect-devices/select-port">Device Connection</SidebarLink>
        </li>
        <li>
          <SidebarLink
            disabled={!canNavigateToDevices}
            to={`/config-devices/${firstDevice && firstDevice.device.deviceNumber}/${firstDevice && firstDevice.axis.axisNumber}`}
          >
            Device Configuration
          </SidebarLink>
        </li>
        {/*<li>
          <SidebarLink disabled={!canNavigateToDevices} to="/device-tuning">Device Tuning</SidebarLink>
        </li>*/}
      </ul>
    );
  }
}

export const Menu = connect(
  (state: State): Partial<Props> => ({
    canNavigateToDevices: selectCanNavigateAndHasDevices(state),
    canNavigate: selectCanNavigate(state),
    firstDevice: selectFirstDevice(state),
  }),
)(MenuBase);
