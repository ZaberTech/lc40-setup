import { createSelector } from 'reselect';

import { selectDetectingDevices, selectHasCompatibleDevices } from '../connect_devices/selectors';
import { selectTestRunning } from '../config_devices/selectors';
import { selectIsSubmitting as selectSubmittingFeedback } from '../feedback/selectors';

export const selectCanNavigate = createSelector(selectDetectingDevices, selectTestRunning, selectSubmittingFeedback,
  (detectingDevices, testRunning, submittingFeedback) => !detectingDevices && !testRunning && !submittingFeedback
);

export const selectCanNavigateAndHasDevices = createSelector(
  selectCanNavigate, selectHasCompatibleDevices,
  (canNavigate, hasDevices) => canNavigate && hasDevices
);
