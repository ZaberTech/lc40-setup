import { applyMiddleware, compose, createStore, Middleware, StoreEnhancer } from 'redux';
import reduxLogger from 'redux-logger';
import createSagaMiddleware from 'redux-saga';
import storeFreeze from 'redux-freeze';
import { routerMiddleware } from 'connected-react-router';
import _ from 'lodash';

import { environment } from '../environment';
import { RecursivePartial } from '../utils';
import { handleError } from '../errors';
import { getLogger } from '../log';

import { rootReducer } from './reducer';
import { rootSaga } from './saga';
import { browserHistory } from './history';
import { State } from './reducer';
import { sentryBreadcrumb } from './sentry';

declare global {
  interface Window {
    __REDUX_DEVTOOLS_EXTENSION__: () => StoreEnhancer;
  }
}

export const buildStore = (preloadedState?: RecursivePartial<State>) => {
  const logger = getLogger('redux');

  const sagaMiddleware = createSagaMiddleware({
    onError: (err, info) => {
      handleError(err);
      logger.error(err, info);
    },
  });

  const middlewares: Middleware[] = [
    sentryBreadcrumb,
    sagaMiddleware,
    routerMiddleware(browserHistory),
  ];
  if (!environment.isProduction) {
    if (!environment.isTest) {
      middlewares.push(reduxLogger);
    }
    middlewares.push(storeFreeze);
  }

  const store = createStore(
    rootReducer,
    (preloadedState as unknown) || {},
    compose(
      window.__REDUX_DEVTOOLS_EXTENSION__ ? window.__REDUX_DEVTOOLS_EXTENSION__() : _.identity,
      applyMiddleware(...middlewares),
    ),
  );

  sagaMiddleware.run(rootSaga);

  return store;
};
