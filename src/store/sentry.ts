import * as Sentry from '@sentry/react';
import { Middleware } from 'redux';

export const sentryBreadcrumb: Middleware = ({ getState }) => next => action => {
  Sentry.addBreadcrumb({
    category: 'redux',
    data: {
      reduxAction: JSON.stringify(action),
      reduxState: JSON.stringify(getState()),
    }
  });

  return next(action);
};
