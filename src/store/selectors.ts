import { State } from './reducer';

export const selectConnectedDevices = (state: State) => state.connectDevices;
export const selectConfigDevices = (state: State) => state.configDevices;
export const selectRouter = (state: State) => state.router;
export const selectSplashScreen = (state: State) => state.splashScreen;
export const selectFeedback = (state: State) => state.feedback;
