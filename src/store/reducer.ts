import { combineReducers } from 'redux';
import { connectRouter, RouterState } from 'connected-react-router';

import { connectDevicesReducer, ConnectDevicesState } from '../connect_devices';
import { configDevicesReducer, ConfigDevicesState } from '../config_devices';
import { splashScreenReducer, SplashScreenState } from '../splash_screen';
import { feedbackReducer, FeedbackState } from '../feedback';

import { browserHistory } from './history';

export interface State {
  connectDevices: ConnectDevicesState;
  configDevices: ConfigDevicesState;
  router: RouterState;
  splashScreen: SplashScreenState;
  feedback: FeedbackState;
}

export const rootReducer = combineReducers<State>({
  connectDevices: connectDevicesReducer,
  configDevices: configDevicesReducer,
  router: connectRouter(browserHistory),
  splashScreen: splashScreenReducer,
  feedback: feedbackReducer,
});
