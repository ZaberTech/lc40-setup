import { all, fork } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';

import { connectDevicesSaga } from '../connect_devices';
import { configDevicesSaga } from '../config_devices';
import { splashScreenSaga } from '../splash_screen';
import { feedbackSaga } from '../feedback';

export function* rootSaga(): SagaIterator {
  yield all([
    fork(connectDevicesSaga),
    fork(configDevicesSaga),
    fork(splashScreenSaga),
    fork(feedbackSaga),
  ]);
}
