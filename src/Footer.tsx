import React from 'react';

import zaberLogo from './pictures/zaber-logo.png';
import { ExternalLink, Link } from './components';
import { FEEDBACK_URL } from './feedback';

export const Footer = () => (
  <div className="layout footer">
    <span className="item"><ExternalLink url="https://www.zaber.com/manuals/LC40B-KM">Device Manual</ExternalLink></span>
    <span className="item">
      <ExternalLink url="https://www.zaber.com/ufs/PdfsStore/7pWsJjaMajkK6H2oc/LC40-parts-and-accessories.pdf">
        Part Glossary
      </ExternalLink>
    </span>
    <span className="item"><ExternalLink url="https://www.zaber.com/contact">Help</ExternalLink></span>
    <span className="item"><Link to="/splash">Welcome page</Link></span>
    <span className="item"><Link to={FEEDBACK_URL}>Feedback</Link></span>
    <img src={zaberLogo} alt="Zaber logo"/>
  </div>
);
