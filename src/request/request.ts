import { injectable, inject } from 'inversify';

import { Log, Logger } from '../log';

import { HttpError } from './http_error';

export interface RequestOptions {
  headers?: { [key: string]: string };
  search?: string[][] | Record<string, string>;
  formUrlEncoded?: true;
}

@injectable()
export class Request {
  private log: Logger;

  constructor(@inject(Log) log: Log) {
    this.log = log.getLogger('Requests');

    this.request = this.request.bind(this);
  }

  public async request<Return = unknown>(url: string, method?: string, body?: unknown, options?: RequestOptions): Promise<Return> {
    if (options && options.search) {
      url = `${url}?${new URLSearchParams(options.search).toString()}`;
    }
    const headers: { [key: string]: string } = {
      ...(options && options.headers)
    };
    if (body) {
      if (options && options.formUrlEncoded === true) {
        headers['Content-Type'] = 'application/x-www-form-urlencoded';
        body = new URLSearchParams(body as Record<string, string>).toString();
      } else if (body instanceof FormData) {
        headers['Content-Type'] = 'multipart/form-data';
      } else {
        headers['Content-Type'] = 'application/json';
        body = JSON.stringify(body);
      }
    }
    const fetchOptions = {
      method: method || 'GET',
      body: body as BodyInit,
      headers: new Headers(headers),
    };
    const response = await fetch(url, fetchOptions);
    const { status } = response;
    this.log.info(fetchOptions.method, url, `-> ${status}`);

    const isError = status < 200 || 299 < status;
    const contentType = response.headers.get('Content-Type');

    let content: unknown;
    try {
      if (status === 204) {
        content = null;
      } else if (contentType && contentType.startsWith('application/json')) {
        content = await response.json();
      } else {
        content = await response.text();
      }
    } catch (contentError) {
      if (!isError) {
        throw contentError;
      }
    }
    if (isError) {
      this.log.warn(`Request error status: ${status}`, content);
      throw new HttpError(`Request error status: ${status}`, status, content);
    }
    return content as Return;
  }
}
