export class HttpError extends Error {
  constructor(
    message: string,
    public readonly status?: number,
    public readonly result?: unknown,
  ) {
    super(message);
    Object.setPrototypeOf(this, HttpError.prototype);
  }
}
