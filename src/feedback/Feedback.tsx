import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';

import { State } from '../store/reducer';
import mehPicture from '../pictures/meh.png';
import likeItPicture from '../pictures/like_it.png';
import { Link, Button } from '../components';
import { Content } from '../layout';
import { ReactComponent as ConfirmationIcon } from '../pictures/confirmation.svg';

import { selectFeedbackData, selectIsSubmitting, selectSubmissionResult, selectDevicesConfigured, selectIsValid, } from './selectors';
import { actions as actionsDefinition } from './actions';
import { FeedbackData } from './types';

interface Props {
  actions: typeof actionsDefinition;
  feedback: FeedbackData;
  devicesConfigured: boolean;
  isSubmitting: boolean;
  result: ReturnType<typeof selectSubmissionResult>;
  isValid: boolean;
}

class FeedbackBase extends React.Component<Props> {
  render(): React.ReactNode {
    const { feedback, actions, isSubmitting, result, devicesConfigured, isValid } = this.props;
    if (result && result.ok) {
      return (
        <Content className="feedback-thank-you">
          <ConfirmationIcon />
          <p>Thank you for your feedback!</p>
        </Content>
      );
    }

    return (
      <Content className="feedback">
        {devicesConfigured && <div className="devices-configured">
          <div className="inner-container">
            <div className="icon-container">
              <ConfirmationIcon />
            </div>
            <p>Your devices have been configured successfully!</p>
          </div>
        </div>}

        <h3>Tell us your experience today</h3>
        <div className="experience">
          <button
            disabled={isSubmitting}
            className={classNames({ selected: feedback.ok === false, button: true, image: true })}
            onClick={() => actions.changeFeedback({ ok: false })}
          >
            <img src={mehPicture} alt="bad experience"/>
            Meh!
          </button>
          <button
            disabled={isSubmitting}
            className={classNames({ selected: feedback.ok === true, button: true, image: true })}
            onClick={() => actions.changeFeedback({ ok: true })}
          >
            <img src={likeItPicture} alt="good experience"/>
            Like it!
          </button>
        </div>

        <div className="comment">
          <p>Do you have some additional comments for us?</p>
          <textarea disabled={isSubmitting} value={feedback.comment} onChange={e => actions.changeFeedback({ comment: e.target.value })} />
        </div>

        <div className="submit">
          {devicesConfigured &&
          <Link disabled={isSubmitting} to="/splash">
            <button disabled={isSubmitting} className="button clear-border">Skip</button>
          </Link>}
          <Button
            data-testid="submit-feedback"
            disabled={isSubmitting || !isValid}
            className="button orange"
            onClick={() => actions.submitFeedback()}
          >Submit</Button>
        </div>
        {result && result.err && <p>Cannot submit the feedback. Please try again later. ({result.err})</p>}
      </Content>
    );
  }
}

export const Feedback = connect(
  (state: State): Partial<Props> => ({
    feedback: selectFeedbackData(state),
    devicesConfigured: selectDevicesConfigured(state),
    isSubmitting: selectIsSubmitting(state),
    result: selectSubmissionResult(state),
    isValid: selectIsValid(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(FeedbackBase);
