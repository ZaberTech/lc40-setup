import React from 'react';
import { injectable, Container } from 'inversify';
import { render, RenderResult } from '@testing-library/react';
import { waitFor, fireEvent } from '@testing-library/dom';
import { routerActions } from 'connected-react-router';

import { wrapWithNewStore, wrapWithRouter, waitTick, defer } from '../test';
import { createContainer, destroyContainer } from '../container';
import { Request } from '../request';

import { Feedback } from './Feedback';
import { REDIRECT_AFTER_SUBMIT } from './sagas';
import { FEEDBACK_URL, FEEDBACK_URL_ALL_CONFIGURED } from './types';

const TestFeedback = wrapWithNewStore(wrapWithRouter(Feedback));

jest.useFakeTimers();

let requestMock: RequestMock;

@injectable()
class RequestMock {
  public request = jest.fn(() => Promise.resolve());

  constructor() {
    requestMock = this;
  }
}

let container: Container;

beforeAll(() => {
  container = createContainer();
  container.bind<unknown>(Request).to(RequestMock);
});

afterAll(() => {
  destroyContainer();
  requestMock = null;
});

let wrapper: RenderResult;

beforeEach(() => {
  if (requestMock) {
    requestMock.request.mockClear();
  }

  wrapper = render(<TestFeedback />);
  TestFeedback.testStore.dispatch(routerActions.replace(FEEDBACK_URL_ALL_CONFIGURED));
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null;

  jest.clearAllTimers();
});

test('feedback cannot be sent without selecting experience', async () => {
  expect(wrapper.getByTestId('submit-feedback')).toHaveAttribute('disabled');
});

test('sends the feedback and redirects after', async () => {
  fireEvent.click(wrapper.getByAltText('bad experience'));
  fireEvent.click(wrapper.getByAltText('good experience'));
  fireEvent.change(wrapper.getByRole('textbox'), { target: { value: 'Still meh!' } });
  fireEvent.click(wrapper.getByTestId('submit-feedback'));

  await waitFor(() =>
    wrapper.getByText('Thank you for your feedback!')
  );

  expect(requestMock.request).toHaveBeenCalledTimes(1);
  expect(requestMock.request.mock.calls[0]).toEqual([
    'https://api.test.io/feedback',
    'POST',
    {
      experience: 'OK',
      comment: 'Still meh!',
      product: 'Product',
      version: 'Test',
    }
  ]);

  jest.advanceTimersByTime(REDIRECT_AFTER_SUBMIT);
  await waitTick();
  expect(TestFeedback.testStore.getState().router.location.pathname).toBe('/splash');
});

test('disables controls while submitting', async () => {
  const deferred = defer<void>();
  requestMock.request.mockImplementationOnce(() => deferred.promise);

  fireEvent.click(wrapper.getByAltText('good experience'));
  fireEvent.click(wrapper.getByTestId('submit-feedback'));

  await waitFor(() =>
    expect(wrapper.getByTestId('submit-feedback')).toHaveAttribute('disabled')
  );

  expect(wrapper.getByAltText('good experience').parentElement).toHaveAttribute('disabled');
  expect(wrapper.getByAltText('bad experience').parentElement).toHaveAttribute('disabled');
  expect(wrapper.getByRole('textbox')).toHaveAttribute('disabled');

  deferred.resolve();
});

test('resets state after location change', async () => {
  fireEvent.click(wrapper.getByAltText('bad experience'));
  expect(wrapper.getByTestId('submit-feedback')).not.toHaveAttribute('disabled');

  TestFeedback.testStore.dispatch(routerActions.replace(FEEDBACK_URL));

  expect(wrapper.getByTestId('submit-feedback')).toHaveAttribute('disabled');
});

test('shows error when the submit fails and allows to repeat', async () => {
  requestMock.request.mockImplementationOnce(() => Promise.reject(new Error('Failure')));

  fireEvent.click(wrapper.getByAltText('good experience'));
  fireEvent.click(wrapper.getByTestId('submit-feedback'));

  await waitFor(() =>
    expect(wrapper.getByText(/Failure/)).toBeDefined()
  );

  fireEvent.click(wrapper.getByTestId('submit-feedback'));

  await waitFor(() =>
    wrapper.getByText('Thank you for your feedback!')
  );
});
