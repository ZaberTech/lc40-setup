export interface FeedbackData {
  ok: boolean;
  comment: string;
}

export const FEEDBACK_URL = '/feedback';
export const DEVICES_CONFIGURED_PARAM = 'devices_configured';
export const FEEDBACK_URL_ALL_CONFIGURED = `${FEEDBACK_URL}?${DEVICES_CONFIGURED_PARAM}`;
