import { all, takeLatest, select, put, delay, race, call, take } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { LOCATION_CHANGE, routerActions } from 'connected-react-router';

import { getContainer } from '../container';
import { Request } from '../request';
import { environment } from '../environment';

import { ActionTypes, actions } from './actions';
import { FeedbackData } from './types';
import { selectFeedbackData, selectDevicesConfigured } from './selectors';

export const REDIRECT_AFTER_SUBMIT = 3000;

export function* feedbackSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.SUBMIT_FEEDBACK, submitFeedback),
  ]);
}

function* redirectAfterFeedback(): SagaIterator {
  yield delay(REDIRECT_AFTER_SUBMIT);
  if (yield select(selectDevicesConfigured)) {
    yield put(routerActions.replace('/splash'));
  } else {
    yield put(routerActions.goBack());
  }
}

function* submitFeedback(): SagaIterator {
  const feedback: FeedbackData = yield select(selectFeedbackData);
  try {
    const requests = getContainer().get<Request>(Request);
    yield call(requests.request, `${environment.apiUrl}/feedback`, 'POST', {
      product: environment.productName,
      version: environment.releaseName,
      experience: feedback.ok ? 'OK' : 'NOK',
      comment: feedback.comment,
    });

    yield put(actions.submitFeedbackDone());
  } catch (err) {
    yield put(actions.submitFeedbackDone(`${err}`));
    return;
  }

  yield race({
    redirect: call(redirectAfterFeedback),
    cancel: take(LOCATION_CHANGE),
  });
}
