import { actionBuilder } from '../utils';

import { FeedbackData } from './types';

export const enum ActionTypes {
  CHANGE_FEEDBACK = 'CHANGE_FEEDBACK',
  SUBMIT_FEEDBACK = 'SUBMIT_FEEDBACK',
  SUBMIT_FEEDBACK_DONE = 'SUBMIT_FEEDBACK_DONE',
}

export interface ActionsToPayloads {
  [ActionTypes.CHANGE_FEEDBACK]: { feedback: Partial<FeedbackData> };
  [ActionTypes.SUBMIT_FEEDBACK]: void;
  [ActionTypes.SUBMIT_FEEDBACK_DONE]: { err?: string };
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  changeFeedback: (feedback: Partial<FeedbackData>) => buildAction(ActionTypes.CHANGE_FEEDBACK, { feedback }),
  submitFeedback: () => buildAction(ActionTypes.SUBMIT_FEEDBACK),
  submitFeedbackDone: (err?: string) => buildAction(ActionTypes.SUBMIT_FEEDBACK_DONE, { err }),
};
