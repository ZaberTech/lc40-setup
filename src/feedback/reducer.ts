import { matchPath } from 'react-router-dom';
import { LocationChangePayload, LOCATION_CHANGE } from 'connected-react-router';

import { createReducer } from '../utils';

import { ActionsToPayloads, ActionTypes } from './actions';
import { FeedbackData, DEVICES_CONFIGURED_PARAM, FEEDBACK_URL } from './types';

export interface State {
  feedback: FeedbackData;
  devicesConfigured: boolean;
  isSubmitting: boolean;
  submitted: { ok: boolean; err?: string; };
}

const initialState: State = {
  feedback: {
    ok: null,
    comment: '',
  },
  devicesConfigured: false,
  isSubmitting: false,
  submitted: null,
};

const changeFeedback = (state: State, { feedback }: ActionsToPayloads[ActionTypes.CHANGE_FEEDBACK]): State => ({
  ...state,
  feedback: { ...state.feedback, ...feedback },
});

const submitFeedback = (state: State): State => ({
  ...state,
  isSubmitting: true,
  submitted: null,
});

const submitFeedbackDone = (state: State, { err }: ActionsToPayloads[ActionTypes.SUBMIT_FEEDBACK_DONE]): State => ({
  ...state,
  isSubmitting: false,
  submitted: { ok: !err, err },
});


const locationChange = (state: State, payload: LocationChangePayload): State => {
  const match = matchPath(payload.location.pathname, FEEDBACK_URL);
  if (!match) {
    return state;
  }

  const params = new URLSearchParams(payload.location.search);
  return { ...initialState, devicesConfigured: params.has(DEVICES_CONFIGURED_PARAM) };
};

type ReducersType = ActionsToPayloads & { [LOCATION_CHANGE]: LocationChangePayload };

export const reducer = createReducer<ReducersType, typeof initialState>({
  [ActionTypes.CHANGE_FEEDBACK]: changeFeedback,
  [ActionTypes.SUBMIT_FEEDBACK]: submitFeedback,
  [ActionTypes.SUBMIT_FEEDBACK_DONE]: submitFeedbackDone,
  [LOCATION_CHANGE]: locationChange,
}, initialState);
