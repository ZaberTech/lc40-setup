import { createSelector } from 'reselect';

import { selectFeedback as selectFeedbackRoot } from '../store';

export const selectFeedbackData = createSelector(selectFeedbackRoot, state => state.feedback);
export const selectIsSubmitting = createSelector(selectFeedbackRoot, state => state.isSubmitting);
export const selectSubmissionResult = createSelector(selectFeedbackRoot, state => state.submitted);
export const selectDevicesConfigured = createSelector(selectFeedbackRoot, state => state.devicesConfigured);
export const selectIsValid = createSelector(selectFeedbackData, feedback => typeof feedback.ok === 'boolean');
