import 'reflect-metadata';
import '@testing-library/jest-dom/extend-expect';

let consoleErrorSpy: jest.SpyInstance;

beforeEach(done => {
  consoleErrorSpy = jest.spyOn(global.console, 'error').mockImplementation(e => {
    done.fail(e);
  });
  done();
});

afterEach(() => {
  consoleErrorSpy.mockClear();
});
