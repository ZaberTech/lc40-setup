import React from 'react';
import { ConnectedRouter } from 'connected-react-router';

import { browserHistory } from '../store/history';

export const wrapWithRouter = <Props extends Record<string, unknown>>(Component: React.ComponentType<Props>) => {
  class WrappedWithRouter extends React.Component<Props> {
    public render(): React.ReactNode {
      return (
        <ConnectedRouter history={browserHistory}>
          <Component {...this.props} />
        </ConnectedRouter>
      );
    }
  }

  return WrappedWithRouter;
};
