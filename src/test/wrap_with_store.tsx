import React from 'react';
import { Provider } from 'react-redux';
import _ from 'lodash';

import { buildStore } from '../store/build';
import { RootState } from '../store';
import { RecursivePartial } from '../utils';

interface PropsForWrapper {
  preloadedState?: RecursivePartial<RootState>;
  store?: ReturnType<typeof buildStore>;
}

export const wrapWithNewStore = <Props extends Record<string, unknown>>(Component: React.ComponentType<Props>) => {
  class WrappedWithStore extends React.Component<Props & PropsForWrapper> {
    private store: ReturnType<typeof buildStore>;

    public static testStore: ReturnType<typeof buildStore>;

    constructor(props: Props & PropsForWrapper) {
      super(props);
      this.store = props.store || buildStore(props.preloadedState);
      WrappedWithStore.testStore = this.store;
    }

    public componentWillUnmount(): void {
      WrappedWithStore.testStore = null;
    }

    public render(): React.ReactNode {
      const props = _.omit(this.props, 'preloadedState', 'store') as Props;
      return (
        <Provider store={this.store}>
          <Component {...props}/>
        </Provider>
      );
    }
  }

  return WrappedWithStore;
};
