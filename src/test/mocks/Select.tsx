/* eslint-disable jsx-a11y/aria-role */
import React from 'react';

interface Value { value: string; label: string; }

interface Props {
  options: Value[];
  onChange: (selectedValue: Value, triggeredAction: { action: string }) => void;
  value: Value;
  isClearable: boolean;
}

function Select({ options, onChange, value, isClearable }: Props): React.ReactNode {
  return (
    <React.Fragment>
      <select
        data-testid="select-mock"
        onChange={e => onChange(options.find(option => option.value === e.target.value), { action: 'select-option' })}
        data-value={value && value.value}
        data-isclearable={isClearable}
      >
        {options && options.map(option => (
          <option key={option.value} value={option.value} data-testid="select-mock-option">{option.label}</option>
        ))}
      </select>
      {isClearable &&
        <button data-testid="select-mock-clear"
          onClick={() => onChange(options.find(option => option.value === value.value), { action: 'clear' })}
        >Clear</button>
      }
    </React.Fragment>
  );
}

jest.mock('react-select', () => Select);
