import { injectable } from 'inversify';

@injectable()
export class TimeMeasuringMock {
  public mockTime: number;
  public now(): number {
    return this.mockTime || performance.now();
  }
}
