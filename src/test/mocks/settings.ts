export class SettingsMock {
  public values: { [key: string]: { value: number, units?: string } } = {};

  public readonly get = jest.fn(this.getMock.bind(this));
  public readonly set = jest.fn(this.setMock.bind(this));

  public getMock(key: string): Promise<number> {
    if (!(key in this.values)) {
      throw new Error(`Unknown settings ${key}`);
    }
    return Promise.resolve(this.values[key].value);
  }

  public async setMock(key: string, value: number, units: string): Promise<void> {
    this.values[key] = { value, units };
  }
}
