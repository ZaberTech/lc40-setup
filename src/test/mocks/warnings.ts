import _ from 'lodash';

export class WarningsMock {
  public warnings: { [key: string]: boolean } = {};
  public readonly getFlags = jest.fn(this.getFlagsMock.bind(this));

  public async getFlagsMock(): Promise<Set<string>> {
    return new Set(_.toPairs(this.warnings).filter((([, present]) => present)).map(([flag]) => flag));
  }
}
