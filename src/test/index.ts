export { wrapWithNewStore } from './wrap_with_store';
export { wrapWithRouter } from './wrap_with_router';

export interface Defer<T> {
  resolve: (value: T) => void;
  reject: (value: unknown) => void;
  promise: Promise<T>;
}

export function defer<T>(): Defer<T> {
  const deferInstance: Partial<Defer<T>> = {};
  deferInstance.promise = new Promise<T>((resolve, reject) => {
    deferInstance.resolve = resolve;
    deferInstance.reject = reject;
  });
  return deferInstance as Defer<T>;
}

export async function waitUntil(condition: () => boolean): Promise<void> {
  await new Promise(resolve => {
    const check = () => {
      if (condition()) {
        resolve();
      } else {
        process.nextTick(check);
      }
    };

    check();
  });
}
export async function waitUntilPass(callback: () => void, timeout: number = 50): Promise<void> {
  const startTS = performance.now();
  await new Promise((resolve, reject) => {
    const check = () => {
      try {
        callback();
        resolve();
      } catch (err) {
        const isLate = (performance.now() - startTS) > timeout;
        if (isLate) {
          return reject(err);
        }
        process.nextTick(check);
      }
    };

    check();
  });
}

export async function waitTick(ticks: number = 1): Promise<void> {
  let ticksLeft = ticks;
  await waitUntil(() => ticksLeft-- <= 0);
}
