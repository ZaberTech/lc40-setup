import React from 'react';
import classNames from 'classnames';

interface Props extends Pick<React.HTMLAttributes<HTMLDivElement>, 'className'> {
  flex?: boolean;
}

export const Content: React.FC<Props> = ({ children, className, flex }) =>
  <div className={classNames('plain-content', className, {
    flex: typeof flex === 'boolean' ? flex : true,
  })}>{children}</div>;
