import React from 'react';
import classNames from 'classnames';

type BasicProps = Pick<React.HTMLAttributes<HTMLDivElement>, 'className'>;

const TabContainer: React.FC<BasicProps> = ({ children, className }) => (
  <div className={classNames('tabs', className)}>{React.Children.toArray(children).reverse()}</div>
);

interface TabProps extends Pick<React.HTMLAttributes<HTMLDivElement>, 'className' | 'onClick'> {
  index: number;
  selected?: boolean;
  disabled?: boolean;
}

const Tab: React.FC<TabProps> = ({ children, selected, index, disabled, onClick, className }) => (
  <div  className={classNames(className, { selected }, 'tab')} style={{ order: index }}>
    <div className="tab-inner">
      <div className="tab-shadow"/>
      <div className="tab-content">
        {children}
      </div>
    </div>
    <div className="click-overlay" onClick={!disabled ? onClick : null}/>
  </div>
);

interface TabContentProps extends Pick<React.HTMLAttributes<HTMLDivElement>, 'className'> {
  flex?: boolean;
}

const TabContent: React.FC<TabContentProps> = ({ children, flex, className }) => (
  <div className="tab-main">
    <div className="tab-shadow" />
    <div className={classNames({ flex: typeof flex === 'boolean' ? flex : true, }, 'tab-main-content', className)}>
      {children}
    </div>
  </div>
);

export class TabLayout extends React.Component<BasicProps> {
  static Tab = Tab;
  static TabContainer = TabContainer;
  static TabContent = TabContent;

  public render(): React.ReactNode {
    const { children, className } = this.props;
    return <div className={classNames('tab-layout', className)}>
      {children}
    </div>;
  }
}
