interface Environment {
  isProduction: boolean;
  isRelease: boolean;
  isTest: boolean;
  releaseName: string;
  apiUrl: string;
  productName: string;
}

export const environment = ((): Environment => {
  const isTest = !!process.env.JEST_WORKER_ID;

  if (isTest) {
    return {
      isProduction: false,
      isRelease: false,
      isTest,
      releaseName: 'Test',
      apiUrl: 'https://api.test.io',
      productName: 'Product',
    };
  }

  return {
    isProduction: process.env.NODE_ENV === 'production',
    isRelease: process.env.RELEASE_NAME.startsWith('release'),
    isTest,
    releaseName: process.env.RELEASE_NAME,
    apiUrl: process.env.API_URL,
    productName: 'Zaber LC40 Setup',
  };
})();
