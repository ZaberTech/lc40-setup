declare module 'enzyme-adapter-react-16';

declare module '*.jpg' {
  const content: string;
  export default content;
}

declare module '*.png' {
  const content: string;
  export default content;
}

declare module '*.svg' {
  const content: string;
  export default content;

  const ReactComponent: new () => React.Component<{ className?: string; }>;
  export { ReactComponent };
}

declare module '*.json' {
  const content: string;
  export default content;
}
