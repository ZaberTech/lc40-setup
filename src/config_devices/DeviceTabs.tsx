import React from 'react';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import { Dispatch, bindActionCreators } from 'redux';

import { State } from '../store/reducer';
import { TabLayout } from '../layout';

import { isSameDeviceAxis, DeviceAxisState, DeviceAxisNumbers } from './types';
import { selectCurrent, selectTestRunning, selectDeviceAxesStates } from './selectors';
import { actions as actionsDefinition } from './actions';

interface Props {
  actions: typeof actionsDefinition;
  currentDeviceAxis: DeviceAxisNumbers;
  deviceAxesStates: DeviceAxisState[];
  testRunning: boolean;
}

const DeviceTabsBase: React.FC<Props> = (props: Props) => {
  const { currentDeviceAxis, deviceAxesStates, testRunning, actions } = props;
  return (
    <TabLayout.TabContainer>
      {deviceAxesStates && deviceAxesStates.map((deviceAxisState, i) => {
        const { device, axis, state } = deviceAxisState;
        const isCurrent = isSameDeviceAxis(currentDeviceAxis, state && state.id);
        return (
          <TabLayout.Tab
            key={`${device.deviceNumber}-${axis.axisNumber}`}
            className="device-tab"
            index={i} selected={isCurrent} disabled={testRunning}
            onClick={() => actions.goToDevice(device.deviceNumber, axis.axisNumber)}>
            <div>
              {state && state.writtenAndTested && <span className="checkmark"><FontAwesomeIcon icon={faCheck}/></span>}
              <span className="device-number">{device.deviceNumber.toString().padStart(2, '0')}</span>
              <span className="name">{device.name}</span>
            </div>
            <div>
              <span className="serial">SN: {device.serialNumber}</span>
            </div>
          </TabLayout.Tab>
        );
      })}
    </TabLayout.TabContainer>
  );
};

export const DeviceTabs = connect(
  (state: State): Partial<Props> => ({
    currentDeviceAxis: selectCurrent(state),
    deviceAxesStates: selectDeviceAxesStates(state),
    testRunning: selectTestRunning(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(DeviceTabsBase);
