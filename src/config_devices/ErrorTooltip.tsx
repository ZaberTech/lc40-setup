import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faExclamationCircle } from '@fortawesome/free-solid-svg-icons';

export const ErrorTooltip: React.FunctionComponent = ({ children }) => (
  <div className="error-tooltip">
    <div className="error-tooltip-container">
      <FontAwesomeIcon icon={faExclamationCircle} />
      <div>
        {children}
      </div>
    </div>
  </div>
);
