import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';

import { State } from '../store/reducer';
import { DeviceAxisPair } from '../connect_devices';
import { Link, Button } from '../components';
import doNotLoadImg from '../pictures/do_not_load.png';

import { actions as actionsDefinition } from './actions';
import {
  selectNextDevice, selectPreviousDevice,
  selectTestRunning, selectAllConfigured
} from './selectors';

interface Props {
  actions: typeof actionsDefinition;
  nextDevice: DeviceAxisPair;
  previousDevice: DeviceAxisPair;
  testRunning: boolean;
  allConfigured: boolean;
}

class ButtonsBase extends React.Component<Props> {
  render(): React.ReactNode {
    const { actions, nextDevice, previousDevice, testRunning, allConfigured } = this.props;
    return (
      <div className="button-group">
        <div className="load-warning">
          <mark className="red">Warning!</mark>
          <img src={doNotLoadImg} className="load-img" alt="Diagram of a man putting a weight on a stage" />
          <div>Do not mount anything on top of the stage during testing</div>
        </div>

        {previousDevice ?
          <Link
            to={`/config-devices/${previousDevice.device.deviceNumber}/${previousDevice.axis.axisNumber}`}
            disabled={testRunning}>
            <Button className="clear" disabled={testRunning} leftarrow={true}>Previous Device</Button>
          </Link> :
          <Link
            to={'/connect-devices/detect-devices'}
            disabled={testRunning}>
            <Button className="clear" disabled={testRunning} leftarrow={true}>Previous Step</Button>
          </Link>
        }
        {nextDevice ?
          <Link
            to={`/config-devices/${nextDevice.device.deviceNumber}/${nextDevice.axis.axisNumber}`}
            disabled={testRunning}>
            <Button className="clear" disabled={testRunning} rightarrow={true}>Next Device</Button>
          </Link> :
          <Button
            className="clear"
            disabled={testRunning || !allConfigured}
            onClick={actions.allConfigured}
          >Done</Button>
        }
      </div>
    );
  }
}

export const Buttons = connect(
  (state: State): Partial<Props> => ({
    nextDevice: selectNextDevice(state),
    previousDevice: selectPreviousDevice(state),
    testRunning: selectTestRunning(state),
    allConfigured: selectAllConfigured(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(ButtonsBase);
