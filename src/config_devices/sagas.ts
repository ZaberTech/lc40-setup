import { all, takeLatest, put, call, race, take, delay, select, cancelled } from 'redux-saga/effects';
import { SagaIterator } from 'redux-saga';
import { Length, ascii, Velocity, MovementFailedException, RequestTimeoutException, MotionLibException } from '@zaber/motion';
import { routerActions } from 'connected-react-router';

import { getContainer } from '../container';
import { ZML } from '../zml';
import { DeviceAxisPair, connectDevicesActions } from '../connect_devices';
import { TimeMeasuring } from '../time';
import { getLogger } from '../log';
import { FEEDBACK_URL_ALL_CONFIGURED } from '../feedback';
import { Lock } from '../utils';

import { ActionTypes, actions } from './actions';
import { selectCurrentDevice, selectAxisState } from './selectors';
import { AxisState, MotorOrientation, TestResultType, LOG_NAME } from './types';
import { TestConfigError, TestConfigErrors, ErrorContext, getErrorContext, errorMessages } from './exception';

const processingLock = new Lock();

export function* configDevicesSaga(): SagaIterator {
  yield all([
    takeLatest(ActionTypes.TEST_CONFIG, testConfigStart),
    takeLatest(ActionTypes.ALL_CONFIGURED, allConfigured),
  ]);
}

function* testConfigStart(): SagaIterator {
  const zml = getContainer().get<ZML>(ZML);
  const currentDevice: DeviceAxisPair = yield select(selectCurrentDevice);
  const axis = zml.connection.getDevice(currentDevice.device.deviceNumber).getAxis(currentDevice.axis.axisNumber);

  const { wasCancelled } = yield race({
    test: call(testConfig, axis),
    wasCancelled: take(ActionTypes.TEST_CONFIG_CANCEL),
  });

  if (wasCancelled) {
    const { unlock } = yield call(processingLock.lock);
    unlock();

    yield put(actions.testConfigResult(TestResultType.CANCELLED));
  }
}

const RESET_TIMEOUT_THRESHOLD = 10000;

function* resetDevice(axis: ascii.Axis): SagaIterator {
  const device = axis.device;

  yield call([device, device.genericCommand], 'system reset');

  yield call(waitForDeviceToBecomeResponsive, axis, true);
}

function* waitForDeviceToBecomeResponsive(axis: ascii.Axis, waitForDriver: boolean): SagaIterator {
  const timeMeasuring = getContainer().get(TimeMeasuring);
  const beginning = timeMeasuring.now();
  while (true) {
    const now = timeMeasuring.now();
    const isLate = now - beginning > RESET_TIMEOUT_THRESHOLD;

    try {
      const warnings: Set<string> = yield call([axis.warnings, axis.warnings.getFlags]);
      const driveDisabled = warnings.has(ascii.WarningFlags.DRIVER_DISABLED) || warnings.has(ascii.WarningFlags.DRIVER_DISABLED_NO_FAULT);
      if (!driveDisabled || !waitForDriver) {
        break;
      } else if (isLate) {
        throw new TestConfigError(TestConfigErrors.DRIVER_DISABLED);
      }

      yield delay(500);
    } catch (err) {
      if (err instanceof RequestTimeoutException && !isLate) {
        continue;
      }
      throw err;
    }
  }
}

const SYSTEM_ACCESS_ADVANCED = 2;
export const SAFE_APPROACH_SPEED_MM_PER_S = 100;
export const MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM = 100;
const MIN_TRAVEL_DISTANCE_FOR_HEURISTIC_MM = 150;

function* ensureInFrontOfHomeSensor(axis: ascii.Axis): SagaIterator {
  let carriageMaybeBehindSensor = false;
  try {
    yield call([axis, axis.genericCommand], 'tools gotolimit home neg 1 0');
    yield call([axis, axis.waitUntilIdle]);
  } catch (err) {
    if (err instanceof MovementFailedException) {
      carriageMaybeBehindSensor = true;
    } else {
      throw err;
    }
  }

  if (carriageMaybeBehindSensor) {
    try {
      yield call([axis, axis.genericCommand], 'tools gotolimit home pos 1 0');
      yield call([axis, axis.waitUntilIdle]);
    } catch (err) {
      if (err instanceof MovementFailedException) {
        throw new TestConfigError(TestConfigErrors.CANNOT_FIND_SENSOR);
      }
      throw err;
    }
  }
}

function* homeAndCheckTravelDistance(axis: ascii.Axis, useHeuristic: boolean): SagaIterator {
  try {
    yield call([axis, axis.home]);
  } catch (err) {
    if (err instanceof MovementFailedException) {
      throw new TestConfigError(TestConfigErrors.CANNOT_HOME);
    }
  }

  const startPos = yield call([axis, axis.getPosition], Length.mm);
  try {
    yield call([axis, axis.moveVelocity], SAFE_APPROACH_SPEED_MM_PER_S, Velocity.MILLIMETRES_PER_SECOND);
    yield call([axis, axis.waitUntilIdle]);
  } catch (err) {
    if (err instanceof MovementFailedException) {
      if (!useHeuristic) {
        throw new TestConfigError(TestConfigErrors.WRONG_ORIENTATION_OR_DISTANCE);
      }

      const endPos = yield call([axis, axis.getPosition], Length.mm);
      const distanceTraveledMm = endPos - startPos;

      if (distanceTraveledMm < MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM) {
        throw new TestConfigError(TestConfigErrors.WRONG_ORIENTATION);
      } else {
        throw new TestConfigError(TestConfigErrors.WRONG_DISTANCE);
      }
    }
    throw err;
  }
}

function* verifyOrientationAndTravelDistance(axis: ascii.Axis): SagaIterator {
  const settings = axis.settings;
  const deviceSettings = axis.device.settings;
  const { config }: AxisState = yield select(selectAxisState);

  const originalLimitApproachMaxSpeed = yield call([settings, settings.get], 'limit.approach.maxspeed');
  const originalAccessLevel = yield call([deviceSettings, deviceSettings.get], 'system.access');
  const needHigherAccessLevel = originalAccessLevel < SYSTEM_ACCESS_ADVANCED;

  const { unlock } = yield call(processingLock.lock);
  try {
    if (needHigherAccessLevel) {
      yield call([deviceSettings, deviceSettings.set], 'system.access', SYSTEM_ACCESS_ADVANCED);
    }

    yield call([settings, settings.set], 'driver.dir', config.motorOrientation === MotorOrientation.LEFT ? 0 : 1);
    yield call([settings, settings.set], 'encoder.dir', config.motorOrientation === MotorOrientation.LEFT ? 1 : 0);
    yield call([settings, settings.set], 'limit.max', config.travelDistanceMm, Length.MILLIMETRES);
    yield call([settings, settings.set], 'limit.approach.maxspeed', SAFE_APPROACH_SPEED_MM_PER_S, Velocity.MILLIMETRES_PER_SECOND);

    // reset the device to reset the reference and volatile settings
    yield call(resetDevice, axis);

    yield call(ensureInFrontOfHomeSensor, axis);

    const useHeuristic = config.travelDistanceMm >= MIN_TRAVEL_DISTANCE_FOR_HEURISTIC_MM;
    yield call(homeAndCheckTravelDistance, axis, useHeuristic);
  } finally {
    try {
      if (yield cancelled()) {
        yield call(stopAndClearWarnings, axis);
        yield call(waitForDeviceToBecomeResponsive, axis, false);
      }

      yield call([settings, settings.set], 'limit.approach.maxspeed', originalLimitApproachMaxSpeed);
      if (needHigherAccessLevel) {
        yield call([deviceSettings, deviceSettings.set], 'system.access', originalAccessLevel);
      }
    } finally {
      unlock();
    }
  }
}

function* stopAndClearWarnings(axis: ascii.Axis): SagaIterator {
  try {
    yield call([axis, axis.stop]);
  } catch (err) {
    if (!(err instanceof MotionLibException)) {
      throw err;
    }
  }
}

function* testAtFullSpeed(axis: ascii.Axis): SagaIterator {
  const { unlock } = yield call(processingLock.lock);
  try {
    yield call([axis, axis.home]);
    yield call([axis, axis.moveMax]);
    yield call([axis, axis.moveMin]);
  } catch (err) {
    if (err instanceof MovementFailedException) {
      throw new TestConfigError(TestConfigErrors.CRASHED_DURING_FINAL);
    }
    throw err;
  } finally {
    try {
      if (yield cancelled()) {
        yield call(stopAndClearWarnings, axis);
      }
    } finally {
      unlock();
    }
  }
}

function* testConfig(axis: ascii.Axis): SagaIterator {
  try {
    yield call(stopAndClearWarnings, axis);
    yield call(verifyOrientationAndTravelDistance, axis);
    yield call(testAtFullSpeed, axis);

    yield put(actions.testConfigResult(TestResultType.OK));
  } catch (err) {
    let message;
    let context: ErrorContext;
    if (err instanceof TestConfigError) {
      const testConfigError = (err as TestConfigError);
      context = getErrorContext(testConfigError.error);
      message = errorMessages[testConfigError.error];
    } else if (err instanceof MotionLibException) {
      context = ErrorContext.GENERIC;
      message = [
        'Unexpected error occurred.',
        'Please ensure that the settings are correct.',
        'If the error persists, please contact Zaber support.',
        String(err),
      ].join(' ');
    } else {
      throw err;
    }

    getLogger(LOG_NAME).warn('Test config failed', err);
    yield put(actions.testConfigResult(TestResultType.ERROR, message, context));
  }
}

function* allConfigured(): SagaIterator {
  yield put(connectDevicesActions.closePort(false));
  yield put(routerActions.push(FEEDBACK_URL_ALL_CONFIGURED));
}
