import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faCheck } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';

import { State } from '../store/reducer';
import motorRight from '../pictures/motor_right_cropped.png';
import motorLeft from '../pictures/motor_left_cropped.png';

import { actions as actionsDefinition } from './actions';
import { AxisState, MotorOrientation, TestResult } from './types';
import { selectAxisState, ValidationResult, selectValidation, selectTestRunning, selectTestResult } from './selectors';
import { ErrorContext } from './exception';
import { ErrorTooltip } from './ErrorTooltip';

interface Props {
  actions: typeof actionsDefinition;
  axis: AxisState;
  validation: ValidationResult;
  testRunning: boolean;
  validate: boolean;
  testResult: TestResult;
}

function MotorConfigBase(props: Props): JSX.Element {
  const { actions, axis, testRunning, validate, validation, testResult } = props;
  const { config } = axis;
  const hasError = (validate && validation.motorOrientation) || (testResult && testResult.errContext === ErrorContext.DRIVER_ORIENTATION);
  return (
    <React.Fragment>
      <div className={classNames({ 'item-cell': true, 'error-border': hasError })} >
        <p>Pick your motor configuration:</p>
        <button
          className={classNames({ selected: config.motorOrientation === 'LEFT' }, 'button image motor-orientation')}
          data-testid="motor-left"
          disabled={testRunning}
          onClick={() => actions.changeConfig({ motorOrientation: MotorOrientation.LEFT })}>
          <img
            src={motorLeft}
            className="direction-img"
            alt="Diagram of LC40 stage with motor on left when viewed from the top"
          />Left {config.motorOrientation === 'LEFT' && <FontAwesomeIcon className="checkmark" icon={faCheck} />}
        </button>
        <button
          className={classNames({ selected: config.motorOrientation === 'RIGHT' }, 'button image motor-orientation')}
          data-testid="motor-right"
          disabled={testRunning}
          onClick={() => actions.changeConfig({ motorOrientation: MotorOrientation.RIGHT })}>
          <img
            src={motorRight}
            className="direction-img"
            alt="Diagram of LC40 stage with motor on right when viewed from the top"
          />Right {config.motorOrientation === 'RIGHT' && <FontAwesomeIcon className="checkmark" icon={faCheck} />}
        </button>
      </div>

      {hasError && <ErrorTooltip>
        {validate && validation.motorOrientation}
        {testResult && testResult.errContext === ErrorContext.DRIVER_ORIENTATION && testResult.err}
      </ErrorTooltip>}

      <div className={classNames({ line: true, orange: true, half: !config.motorOrientation })} />
    </React.Fragment>
  );
}


export const MotorConfig = connect(
  (state: State): Partial<Omit<Props, 'validate'>> => ({
    axis: selectAxisState(state),
    testRunning: selectTestRunning(state),
    validation: selectValidation(state),
    testResult: selectTestResult(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(MotorConfigBase);
