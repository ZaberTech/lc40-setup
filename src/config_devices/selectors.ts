import { createSelector } from 'reselect';
import _ from 'lodash';

import { selectConfigDevices } from '../store';
import { selectDevices, selectCompatibleDeviceAxes } from '../connect_devices/selectors';
import { DeviceAxisPair } from '../connect_devices/types';
import { nullGuard } from '../utils';

import { MotorOrientation, MAXIMUM_TRAVEL_LENGTH_MM, isSameDeviceAxis, DeviceAxisState } from './types';

export const selectAxisState = createSelector(selectConfigDevices,
  state => state.current && state.axes.find(axis => isSameDeviceAxis(axis.id, state.current))
);

export const selectCurrent = createSelector(selectConfigDevices, state => state.current);

export const selectCurrentDevice = createSelector(selectDevices, selectCurrent, nullGuard(
  (devices, current): DeviceAxisPair => {
    const device = devices.find(d => d.deviceNumber === current.deviceNumber);
    const axis = device.axes.find(a => a.axisNumber === current.axisNumber);
    return { device, axis };
  }
));

export const selectNextDevice = createSelector(selectCompatibleDeviceAxes, selectCurrent, nullGuard(
  (deviceAxes, current) => {
    const currentIndex = deviceAxes.findIndex(
      ({ device, axis }) => device.deviceNumber === current.deviceNumber && axis.axisNumber === current.axisNumber
    );
    return deviceAxes[currentIndex + 1] || null;
  }
));

export const selectPreviousDevice = createSelector(selectCompatibleDeviceAxes, selectCurrent, nullGuard(
  (deviceAxes, current) => {
    const currentIndex = deviceAxes.findIndex(
      ({ device, axis }) => device.deviceNumber === current.deviceNumber && axis.axisNumber === current.axisNumber
    );
    return deviceAxes[currentIndex - 1] || null;
  }
));

export const selectFirstDevice = createSelector(selectCompatibleDeviceAxes, deviceAxes => deviceAxes && deviceAxes[0]);

export interface ValidationResult {
  isValid: boolean;
  motorOrientation: string;
  travelDistance: string;
}

export const selectValidation = createSelector(selectAxisState, nullGuard(axisState => {
  const config = axisState.config;
  const validation: ValidationResult = {
    isValid: null,
    motorOrientation: null,
    travelDistance: null,
  };
  if (typeof config.travelDistanceMm !== 'number') {
    validation.travelDistance = 'Please enter a valid number.';
  } else if (config.travelDistanceMm <= 0) {
    validation.travelDistance = 'Travel distance must be more than 0.';
  } else if (config.travelDistanceMm > MAXIMUM_TRAVEL_LENGTH_MM) {
    validation.travelDistance = `Travel distance must be less or equal to ${MAXIMUM_TRAVEL_LENGTH_MM} mm.`;
  }

  if (![MotorOrientation.LEFT, MotorOrientation.RIGHT].includes(config.motorOrientation)) {
    validation.motorOrientation = 'Specify motor orientation.';
  }

  validation.isValid = _.values(validation).every(validationProperty => validationProperty === null);

  return validation;
}));

export const selectTestRunning = createSelector(selectConfigDevices, state => state.testRunning);
export const selectTestResult = createSelector(selectConfigDevices, state => state.testResult);

export const selectDeviceAxesStates = createSelector(selectCompatibleDeviceAxes, selectConfigDevices, nullGuard(
  (deviceAxes, axisStates): DeviceAxisState[] => deviceAxes.map(deviceAxis => ({
    ...deviceAxis,
    state: axisStates.axes.find(({ id }) =>
      id.deviceNumber === deviceAxis.device.deviceNumber && id.axisNumber === deviceAxis.axis.axisNumber
    ),
  }))
));

export const selectAllConfigured = createSelector(selectCompatibleDeviceAxes, selectConfigDevices, nullGuard(
  (deviceAxes, state) => deviceAxes.length === state.axes.length && state.axes.every(axisState => axisState && axisState.writtenAndTested)
));
