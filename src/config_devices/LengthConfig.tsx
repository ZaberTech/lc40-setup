import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import classNames from 'classnames';
import ReactTooltip from 'react-tooltip';

import serialImage from '../pictures/serial.png';
import { State } from '../store/reducer';
import { QuestionIcon } from '../components';

import { AxisState, MAXIMUM_TRAVEL_LENGTH_MM, TestResult } from './types';
import { actions as actionsDefinition } from './actions';
import { selectAxisState, ValidationResult, selectValidation, selectTestRunning, selectTestResult } from './selectors';
import { ErrorContext } from './exception';
import { ErrorTooltip } from './ErrorTooltip';

interface Props {
  actions: typeof actionsDefinition;
  axis: AxisState;
  validation: ValidationResult;
  testRunning: boolean;
  validate: boolean;
  testResult: TestResult;
}

function LengthConfigBase(props: Props): JSX.Element {
  const { actions, axis, testRunning, validate, validation, testResult } = props;
  const { config } = axis;
  const hasError = (validate && validation.travelDistance) || (testResult && testResult.errContext === ErrorContext.TRAVEL_DISTANCE);
  return (
    <React.Fragment>
      <div className={classNames({ 'item-cell': true, 'error-border': hasError })} >
        <p>Specify the travel length:</p>
        <input
          type="number" min="0" max={MAXIMUM_TRAVEL_LENGTH_MM} step="0.000001"
          data-testid="travel-distance"
          disabled={testRunning}
          className="length-input"
          value={typeof config.travelDistanceMm === 'number' ? config.travelDistanceMm : ''}
          onChange={({ target: { value } }) => actions.changeConfig({ travelDistanceMm: value !== '' ? +value : Number.NaN })}
        />
        <span>mm</span>

        <QuestionIcon
          data-for="travel-distance-help"
          data-tip={true} />
      </div>

      {hasError && <ErrorTooltip>
        {validate && validation.travelDistance}
        {testResult && testResult.errContext === ErrorContext.TRAVEL_DISTANCE && testResult.err}
      </ErrorTooltip>}

      <div className={classNames({ line: true, orange: config.motorOrientation, half: !config.travelDistanceMm })} />

      <ReactTooltip id="travel-distance-help" className="tooltip travel-distance-tooltip">
        <p>Device maximum travel length can be found on the label.</p>
        <img src={serialImage} alt="LC40 device serial number, last digits signify travel length" />
      </ReactTooltip>
    </React.Fragment>
  );
}


export const LengthConfig = connect(
  (state: State): Partial<Omit<Props, 'validate'>> => ({
    axis: selectAxisState(state),
    testRunning: selectTestRunning(state),
    validation: selectValidation(state),
    testResult: selectTestResult(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(LengthConfigBase);
