import { routerActions } from 'connected-react-router';

import { actionBuilder } from '../utils';

import { TestResultType, AxisConfig } from './types';
import { ErrorContext } from './exception';

export const enum ActionTypes {
  CHANGE_CONFIG = 'CHANGE_CONFIG',
  TEST_CONFIG = 'TEST_CONFIG',
  TEST_CONFIG_CANCEL = 'TEST_CONFIG_CANCEL',
  TEST_RESULT_CLEAR = 'TEST_RESULT_CLEAR',
  TEST_CONFIG_RESULT = 'TEST_CONFIG_RESULT',
  ALL_CONFIGURED = 'ALL_CONFIGURED',
}

export interface ActionsToPayloads {
  [ActionTypes.CHANGE_CONFIG]: { config: Partial<AxisConfig> };
  [ActionTypes.TEST_CONFIG]: void;
  [ActionTypes.TEST_CONFIG_CANCEL]: void;
  [ActionTypes.TEST_RESULT_CLEAR]: void;
  [ActionTypes.TEST_CONFIG_RESULT]: { result: TestResultType, err: string; errContext: ErrorContext };
  [ActionTypes.ALL_CONFIGURED]: void;
}

const buildAction = <K extends keyof ActionsToPayloads>(type: K, value?: ActionsToPayloads[K]) => actionBuilder(type, value);

export const actions = {
  changeConfig: (config: Partial<AxisConfig>) => buildAction(ActionTypes.CHANGE_CONFIG, { config }),
  testConfig: () => buildAction(ActionTypes.TEST_CONFIG),
  testResultClear: () => buildAction(ActionTypes.TEST_RESULT_CLEAR),
  testConfigCancel: () => buildAction(ActionTypes.TEST_CONFIG_CANCEL),
  testConfigResult: (result: TestResultType, err?: string, errContext?: ErrorContext) =>
    buildAction(ActionTypes.TEST_CONFIG_RESULT, { result, err, errContext }),
  allConfigured: () => buildAction(ActionTypes.ALL_CONFIGURED),
  goToDevice: (deviceNumber: number, axisNumber: number) => routerActions.push(`/config-devices/${deviceNumber}/${axisNumber}`),
};
