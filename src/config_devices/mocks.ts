import { injectable } from 'inversify';

import { SettingsMock } from '../test/mocks/settings';
import { WarningsMock } from '../test/mocks/warnings';

export const ORIGINAL_APPROACH_SPEED = 5544;

export class AxisMock {
  public position: number = 0;
  public readonly settings = new SettingsMock();
  public readonly warnings = new WarningsMock();

  constructor(public readonly device: DeviceMock) {
    this.settings.values['limit.approach.maxspeed'] = { value: ORIGINAL_APPROACH_SPEED };
  }

  public genericCommand = jest.fn().mockResolvedValue({});
  public waitUntilIdle = jest.fn().mockResolvedValue(undefined);
  public moveVelocity = jest.fn().mockResolvedValue(undefined);
  public home = jest.fn().mockResolvedValue(undefined);
  public moveMin = jest.fn().mockResolvedValue(undefined);
  public moveMax = jest.fn().mockResolvedValue(undefined);
  public stop = jest.fn().mockResolvedValue(undefined);
  public getPosition = jest.fn(async () => this.position);
}

export class DeviceMock {
  public readonly settings = new SettingsMock();
  public readonly warnings = new WarningsMock();
  private axes: AxisMock[];

  constructor(public readonly deviceAddress: number) {
    this.axes = [new AxisMock(this)];

    this.settings.values['system.access'] = { value: 1 };
  }

  public getAxis(axisNumber: number): AxisMock {
    return this.axes[axisNumber - 1];
  }

  public genericCommand = jest.fn().mockResolvedValue({});
}

export class ConnectionMock {
  private devices = [new DeviceMock(2)];

  public getDevice(deviceAddress: number): DeviceMock {
    return this.devices.find(device => device.deviceAddress === deviceAddress);
  }
}

@injectable()
export class ZmlMock {
  public connection: ConnectionMock;

  public reset(): void {
    this.connection = new ConnectionMock();
  }

  public close = jest.fn(() => Promise.resolve());
}
