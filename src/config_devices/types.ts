import { Device, Axis } from '../connect_devices';

import { ErrorContext } from './exception';

export const MAXIMUM_TRAVEL_LENGTH_MM = 10000;
export const CONTACT_SUPPORT_FAILED_ATTEMPTS = 3;

export enum MotorOrientation {
  LEFT = 'LEFT',
  RIGHT = 'RIGHT',
}

export enum TestResultType {
  OK = 'OK',
  ERROR = 'ERROR',
  CANCELLED = 'CANCELLED',
}

export interface DeviceAxisNumbers {
  deviceNumber: number;
  axisNumber: number;
}

export interface AxisConfig {
  motorOrientation?: MotorOrientation;
  travelDistanceMm?: number;
}

export interface AxisState {
  id: DeviceAxisNumbers;
  config: AxisConfig;
  writtenAndTested: boolean;
  failedAttempts: number;
}

export const isSameDeviceAxis = (n1: DeviceAxisNumbers, n2: DeviceAxisNumbers) =>
  n1 && n2 && n1.deviceNumber === n2.deviceNumber && n1.axisNumber === n2.axisNumber;

export interface TestResult {
  result: TestResultType;
  err: string;
  errContext: ErrorContext;
}

export interface DeviceAxisState {
  device: Device;
  axis: Axis;
  state: AxisState;
}

export const LOG_NAME = 'config_devices';
