export enum TestConfigErrors {
  CANNOT_FIND_SENSOR = 'CANNOT_FIND_SENSOR',
  CANNOT_HOME = 'CANNOT_HOME',
  WRONG_ORIENTATION = 'WRONG_ORIENTATION',
  WRONG_DISTANCE = 'WRONG_DISTANCE',
  WRONG_ORIENTATION_OR_DISTANCE = 'WRONG_ORIENTATION_OR_DISTANCE',
  CRASHED_DURING_FINAL = 'CRASHED_DURING_FINAL',
  DRIVER_DISABLED = 'DRIVER_DISABLED',
}

export enum ErrorContext {
  GENERIC = 1,
  DRIVER_ORIENTATION,
  TRAVEL_DISTANCE,
}

const testConfigErrorsContext: { [key in TestConfigErrors]?: ErrorContext } = {
  [TestConfigErrors.WRONG_ORIENTATION]: ErrorContext.DRIVER_ORIENTATION,
  [TestConfigErrors.WRONG_DISTANCE]: ErrorContext.TRAVEL_DISTANCE,
};

export const getErrorContext = (error: TestConfigErrors) => testConfigErrorsContext[error] || ErrorContext.GENERIC;

export const errorMessages: { [key in TestConfigErrors]: string } = {
  [TestConfigErrors.CANNOT_FIND_SENSOR]: [
    'Cannot find the home sensor.',
    'Make sure the home sensor is mounted and plugged.',
  ].join(' '),
  [TestConfigErrors.CANNOT_HOME]: [
    'Device cannot home.',
    'Make sure that the home sensor is mounted and plugged properly.'
  ].join(' '),
  [TestConfigErrors.WRONG_ORIENTATION]: [
    'Motor orientation appears to be incorrect.',
    'Make sure that the correct orientation is selected.'
  ].join(' '),
  [TestConfigErrors.WRONG_DISTANCE]: [
    'The travel length that you entered seems too long.',
    'Make sure that the travel distance matches your product.'
  ].join(' '),
  [TestConfigErrors.WRONG_ORIENTATION_OR_DISTANCE]: [
    'The travel length or motor orientation is incorrect.',
    'Make sure that the correct orientation is selected.',
    'Make sure that the travel distance matches your product.'
  ].join(' '),
  [TestConfigErrors.CRASHED_DURING_FINAL]: [
    'Device has crashed during final verification of the settings.',
    'Ensure that the settings are correct.',
  ].join(' '),
  [TestConfigErrors.DRIVER_DISABLED]: [
    'Device has disabled driver.',
    'Enabled the driver in Zaber console and repeat the process.',
  ].join(' '),
};

export class TestConfigError extends Error {
  constructor(
    public readonly error: TestConfigErrors
  ) {
    super(error);
    Object.setPrototypeOf(this, TestConfigError.prototype);
  }
}
