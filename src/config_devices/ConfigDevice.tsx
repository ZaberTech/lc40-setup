import React from 'react';
import { bindActionCreators, Dispatch } from 'redux';
import { connect } from 'react-redux';
import { faPlay, faStop } from '@fortawesome/free-solid-svg-icons';
import classNames from 'classnames';

import { State } from '../store/reducer';
import { DeviceAxisPair } from '../connect_devices';
import { Modal, ButtonWithIcon, Loader } from '../components';
import { TabLayout } from '../layout';

import { actions as actionsDefinition } from './actions';
import { AxisState, DeviceAxisNumbers, isSameDeviceAxis, TestResult, CONTACT_SUPPORT_FAILED_ATTEMPTS } from './types';
import { MotorConfig } from './MotorConfig';
import { LengthConfig } from './LengthConfig';
import {
  selectAxisState, selectCurrentDevice, ValidationResult,
  selectValidation, selectTestRunning, selectTestResult } from './selectors';
import { ErrorContext } from './exception';
import { Buttons } from './Buttons';
import { DeviceTabs } from './DeviceTabs';

interface Props {
  actions: typeof actionsDefinition;
  axis: AxisState;
  currentDevice: DeviceAxisPair;
  validation: ValidationResult;
  testRunning: boolean;
  testResult: TestResult;
}

interface InternalState {
  validate: boolean;
  axisToCompareForReset: DeviceAxisNumbers;
}

class ConfigDeviceBase extends React.Component<Props, InternalState> {
  constructor(props: Props) {
    super(props);
    this.state = ConfigDeviceBase.getInitialState(props);
  }

  static getInitialState(props: Props): InternalState {
    return {
      axisToCompareForReset: props.axis && props.axis.id,
      validate: false,
    };
  }

  static getDerivedStateFromProps(props: Props, state: InternalState): InternalState {
    if (!isSameDeviceAxis(props.axis && props.axis.id, state.axisToCompareForReset)) {
      return ConfigDeviceBase.getInitialState(props);
    }
    return null;
  }

  onTestConfig = () => {
    const { actions, validation } = this.props;
    if (!validation.isValid) {
      this.setState({ validate: true });
    } else {
      actions.testConfig();
    }
  }

  renderTestNote(): React.ReactNode {
    const { axis, testRunning } = this.props;
    if (testRunning) {
      return <mark className="red">You can stop the device any time.</mark>;
    } else if (axis.writtenAndTested) {
      return <mark className="green">Configuration is complete.</mark>;
    } else if (axis.failedAttempts >= CONTACT_SUPPORT_FAILED_ATTEMPTS) {
      return <mark className="red">Please contact Zaber support if you cannot configure the device successfully.</mark>;
    } else {
      return <mark className="orange">Note: Be ready. Your device will move.</mark>;
    }
  }

  render(): React.ReactNode {
    const { actions, axis, currentDevice, validation, testRunning, testResult } = this.props;
    const { validate } = this.state;
    if (!currentDevice) {
      return null;
    }
    const { config, writtenAndTested } = axis;
    return (
      <TabLayout className="config-device">
        <DeviceTabs />
        <TabLayout.TabContent>
          <h3>Setup Configuration</h3>

          <div className="list-wrapper">
            <div className="line gray" />
            <div className="item-wrapper">
              <div className="bullet">1</div>
              <div className="item select-direction">
                <MotorConfig validate={validate} />
              </div>
            </div>

            <div className="item-wrapper">
              <div className={classNames({ bullet: true, unselected: !config.motorOrientation })}>2</div>
              <div className="item length-form">
                <LengthConfig validate={validate} />
              </div>
            </div>

            <div className="item-wrapper">
              <div className={classNames({ bullet: true, unselected: !config.motorOrientation || !config.travelDistanceMm })}>3</div>
              <div className="item start-test">
                <div className="button-container">

                  {!testRunning && <ButtonWithIcon
                    className={'orange no-hover'}
                    data-testid="run-test" onClick={this.onTestConfig} disabled={validate && !validation.isValid}
                    icon={faPlay}>
                    {!writtenAndTested && <React.Fragment>Test Configuration</React.Fragment>}
                    {writtenAndTested && <React.Fragment>Test Again</React.Fragment>}
                  </ButtonWithIcon>}

                  {testRunning && <ButtonWithIcon className="button no-hover"
                    data-testid="stop-test" onClick={actions.testConfigCancel} icon={faStop}>
                      Stop
                  </ButtonWithIcon>}

                  <Loader show={testRunning}/>
                </div>

                {this.renderTestNote()}

                <div className="line background" />
              </div>
            </div>
          </div>

          <Buttons/>

          <Modal title="Oh no! That did not work" type="error"
            isOpen={testResult && testResult.errContext === ErrorContext.GENERIC}
            onRequestClose={actions.testResultClear}>
            <p>{testResult && testResult.errContext === ErrorContext.GENERIC && testResult.err}</p>
          </Modal>
        </TabLayout.TabContent>
      </TabLayout>
    );
  }
}

export const ConfigDevice = connect(
  (state: State): Partial<Props> => ({
    axis: selectAxisState(state),
    currentDevice: selectCurrentDevice(state),
    validation: selectValidation(state),
    testRunning: selectTestRunning(state),
    testResult: selectTestResult(state),
  }),
  (dispatch: Dispatch) => ({
    actions: bindActionCreators(actionsDefinition, dispatch),
  }),
)(ConfigDeviceBase);
