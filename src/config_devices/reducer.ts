
import { LOCATION_CHANGE, LocationChangePayload } from 'connected-react-router';
import { matchPath } from 'react-router';

import { createReducer } from '../utils';
import { ConnectDevicesActionTypes, ConnectDevicesActionPayloads } from '../connect_devices';

import { ActionsToPayloads, ActionTypes } from './actions';
import { AxisState, isSameDeviceAxis, TestResult, TestResultType, DeviceAxisNumbers } from './types';

export interface State {
  axes: AxisState[];
  testRunning: boolean;
  testResult: TestResult;
  current: DeviceAxisNumbers;
}

const initialState: State = {
  axes: [],
  testRunning: false,
  testResult: null,
  current: null,
};

const changeCurrentAxis = (state: State, axisReducer: (axis: AxisState) => AxisState): Partial<State> => ({
  axes: state.axes.map(axis => isSameDeviceAxis(axis.id, state.current) ? axisReducer(axis) : axis),
});

const changeConfig = (state: State, { config }: ActionsToPayloads[ActionTypes.CHANGE_CONFIG]
): State => ({
  ...state,
  ...changeCurrentAxis(state, axis => ({ ...axis, writtenAndTested: false, config: { ...axis.config, ...config } })),
});

const testResultClear = (state: State,): State => ({
  ...state,
  testResult: null,
});

const resetConfig = () => initialState;

const testConfig = (state: State): State => ({
  ...state,
  testResult: null,
  testRunning: true,
  ...changeCurrentAxis(state, axis => ({ ...axis, writtenAndTested: false })),
});

const testConfigResult = (state: State, testResult: ActionsToPayloads[ActionTypes.TEST_CONFIG_RESULT]): State => ({
  ...state,
  testResult,
  testRunning: false,
  ...changeCurrentAxis(state, axis => ({
    ...axis,
    writtenAndTested: testResult.result === TestResultType.OK,
    failedAttempts: testResult.result === TestResultType.ERROR ? axis.failedAttempts + 1 : 0,
  })),
});

const locationChange = (state: State, payload: LocationChangePayload): State => {
  const match = matchPath<{ device: string, axis: string }>(payload.location.pathname, '/config-devices/:device/:axis');
  if (!match) {
    if (state.current) {
      return { ...state, current: null };
    }
    return state;
  }

  const current: DeviceAxisNumbers = { deviceNumber: +match.params.device, axisNumber: +match.params.axis };
  const newState: State = { ...state, current, testResult: null };

  if (!newState.axes.find(axis => isSameDeviceAxis(axis.id, current))) {
    newState.axes = [...newState.axes, { id: current, config: {}, writtenAndTested: false, failedAttempts: 0 }];
  }

  return newState;
};

type ReducersType = ActionsToPayloads & ConnectDevicesActionPayloads & { [LOCATION_CHANGE]: LocationChangePayload };

export const reducer = createReducer<ReducersType, typeof initialState>({
  [ActionTypes.CHANGE_CONFIG]: changeConfig,
  [ConnectDevicesActionTypes.DEVICES_DETECTED]: resetConfig,
  [ActionTypes.TEST_RESULT_CLEAR]: testResultClear,
  [ActionTypes.TEST_CONFIG]: testConfig,
  [ActionTypes.TEST_CONFIG_RESULT]: testConfigResult,
  [LOCATION_CHANGE]: locationChange,
}, initialState);
