import React from 'react';
import { Container } from 'inversify';
import { render, RenderResult, fireEvent, waitFor } from '@testing-library/react';
import * as router from 'connected-react-router';
import { MovementFailedException, RequestTimeoutException, Velocity, ascii } from '@zaber/motion';

import { wrapWithNewStore, wrapWithRouter, waitUntilPass, defer } from '../test';
import { createContainer, destroyContainer } from '../container';
import { ZML } from '../zml';
import { buildStore } from '../store/build';
import { connectDevicesActions } from '../connect_devices';

import { ConfigDevice } from './ConfigDevice';
import { selectTestRunning } from './selectors';
import { ZmlMock, AxisMock, DeviceMock, ORIGINAL_APPROACH_SPEED } from './mocks';
import { SAFE_APPROACH_SPEED_MM_PER_S, MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM } from './sagas';

const TestConfigDevice = wrapWithNewStore(wrapWithRouter(ConfigDevice));

jest.useFakeTimers();

let zmlMock: ZmlMock;
let container: Container;

beforeAll(() => {
  container = createContainer();
  container.bind<unknown>(ZML).to(ZmlMock);
  zmlMock = (container.get(ZML) as unknown) as ZmlMock;
});

afterAll(() => {
  destroyContainer();
});

let wrapper: RenderResult;
let store: ReturnType<typeof buildStore>;

beforeEach(() => {
  store = buildStore({
    connectDevices: {
      devices: [
        { deviceNumber: 2, name: 'X-DEV', axes: [{ axisNumber: 1, isCompatible: true }] },
        { deviceNumber: 3, name: 'X-LRT', axes: [{ axisNumber: 1, isCompatible: true }] },
      ],
    },
  });

  store.dispatch(router.replace('/config-devices/2/1'));

  wrapper = render(<TestConfigDevice store={store} />);
});

afterEach(() => {
  wrapper.unmount();
  wrapper = null;
});

test('renders the correct device', () => {
  wrapper.getByText(/X-DEV/);
});

test('does not crash when port closes', async () => {
  wrapper.getByText(/X-DEV/);

  store.dispatch(connectDevicesActions.closePort(true));

  await waitFor(() =>
    expect(wrapper.queryByText(/X-DEV/)).toBeNull()
  );
});

describe('validation', () => {
  test('clicking test reveals validation and disables the button', () => {
    expect(wrapper.getByTestId('run-test')).not.toHaveAttribute('disabled');

    fireEvent.click(wrapper.getByTestId('run-test'));

    expect(wrapper.getByTestId('run-test')).toHaveAttribute('disabled');
    wrapper.getByText(/Specify motor orientation/);
    wrapper.getByText(/Please enter a valid number/);
  });

  test('validates travel distance', () => {
    fireEvent.click(wrapper.getByTestId('run-test'));

    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 50000 } });
    wrapper.getByText('Travel distance must be less or equal to 10000 mm.');

    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 0 } });
    wrapper.getByText('Travel distance must be more than 0.');

    expect(wrapper.getByTestId('run-test')).toHaveAttribute('disabled');
  });

  test('enables button back once the validation issues are addressed', () => {
    expect(wrapper.getByTestId('run-test')).not.toHaveAttribute('disabled');
    fireEvent.click(wrapper.getByTestId('run-test'));

    expect(wrapper.getByTestId('run-test')).toHaveAttribute('disabled');
    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 150 } });
    fireEvent.click(wrapper.getByTestId('motor-left'));
    expect(wrapper.getByTestId('run-test')).not.toHaveAttribute('disabled');
  });

  test('resets validation display when device changes', () => {
    fireEvent.click(wrapper.getByTestId('run-test'));
    expect(wrapper.getByTestId('run-test')).toHaveAttribute('disabled');

    fireEvent.click(wrapper.getByText(/Next Device/));

    wrapper.getByText(/X-LRT/);
    expect(wrapper.getByTestId('run-test')).not.toHaveAttribute('disabled');
  });
});

describe('testing configuration', () => {
  let device: DeviceMock;
  let axis: AxisMock;

  beforeEach(() => {
    zmlMock.reset();
    device = zmlMock.connection.getDevice(2);
    axis = device.getAxis(1);

    fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 250 } });
    fireEvent.click(wrapper.getByTestId('motor-right'));
  });

  afterEach(async () => {
    await waitUntilPass(() => expect(selectTestRunning(store.getState())).toBe(false));
  });

  async function runTestToEnd(): Promise<void> {
    fireEvent.click(wrapper.getByTestId('run-test'));
    await waitUntilPass(() => expect(selectTestRunning(store.getState())).toBe(false));
  }

  describe('procedure tests', () => {
    test('writes the correct settings', async () => {
      await runTestToEnd();

      expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 1]);
      expect(axis.settings.set.mock.calls).toContainEqual(['encoder.dir', 0]);
      expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 250, 'Length:millimetres']);
    });

    test('writes the correct settings (second case)', async () => {
      fireEvent.change(wrapper.getByTestId('travel-distance'), { target: { value: 150 } });
      fireEvent.click(wrapper.getByTestId('motor-left'));

      await runTestToEnd();

      expect(axis.settings.set.mock.calls).toContainEqual(['driver.dir', 0]);
      expect(axis.settings.set.mock.calls).toContainEqual(['encoder.dir', 1]);
      expect(axis.settings.set.mock.calls).toContainEqual(['limit.max', 150, 'Length:millimetres']);
    });

    test('sets the access level and sets it back to previous level', async () => {
      await runTestToEnd();

      expect(device.settings.set.mock.calls).toEqual([
        ['system.access', 2],
        ['system.access', 1],
      ]);
    });

    test('does not set and unset the access level if it does not need to', async () => {
      device.settings.values['system.access'] = { value: 2 };

      await runTestToEnd();

      expect(device.settings.set).not.toHaveBeenCalled();
    });

    test('resets the device and waits until device starts responding and FD disappears', async () => {
      axis.warnings.warnings[ascii.WarningFlags.DRIVER_DISABLED] = true;
      axis.warnings.getFlags.mockRejectedValueOnce(new RequestTimeoutException('Timeout'));

      fireEvent.click(wrapper.getByTestId('run-test'));

      await waitUntilPass(() =>
        expect(device.genericCommand).toHaveBeenCalledWith('system reset')
      );

      await waitUntilPass(() =>
        expect(axis.warnings.getFlags).toHaveBeenCalledTimes(2)
      );

      axis.warnings.warnings[ascii.WarningFlags.DRIVER_DISABLED] = false;
      jest.advanceTimersByTime(1000);

      await waitUntilPass(() =>
        expect(axis.warnings.getFlags).toHaveBeenCalledTimes(3)
      );

      await waitUntilPass(() => expect(selectTestRunning(store.getState())).toBe(false));
    });

    test('tries to find the sensor', async () => {
      await runTestToEnd();

      expect(axis.genericCommand).toHaveBeenCalledWith('tools gotolimit home neg 1 0');
    });

    test('tries to find the sensor on other side if the first attempt fails', async () => {
      axis.genericCommand.mockImplementation((command: string) => {
        if (command === 'tools gotolimit home neg 1 0') {
          axis.waitUntilIdle.mockRejectedValueOnce(new MovementFailedException('Failed'));
        }
        return Promise.resolve();
      });

      await runTestToEnd();

      expect(axis.genericCommand).toHaveBeenCalledWith('tools gotolimit home pos 1 0');
    });

    test('displays error when cannot find the home sensor', async () => {
      axis.genericCommand.mockImplementation((command: string) => {
        if (['tools gotolimit home neg 1 0', 'tools gotolimit home pos 1 0'].includes(command)) {
          axis.waitUntilIdle.mockRejectedValueOnce(new MovementFailedException('Failed'));
        }
        return Promise.resolve();
      });

      await runTestToEnd();

      wrapper.getByText(/Cannot find the home sensor/);
    });

    test('sets safe approach speed and writes the original approach speed back', async () => {
      await runTestToEnd();

      expect(axis.settings.set.mock.calls).toContainEqual(
        ['limit.approach.maxspeed', SAFE_APPROACH_SPEED_MM_PER_S, 'Velocity:millimetres per second']
      );
      expect(axis.settings.set.mock.calls).toContainEqual(['limit.approach.maxspeed', ORIGINAL_APPROACH_SPEED]);
    });

    test('tests the final configuration by homing and moving to max and min', async () => {
      await runTestToEnd();

      expect(axis.home).toHaveBeenCalled();
      expect(axis.moveMin).toHaveBeenCalled();
      expect(axis.moveMax).toHaveBeenCalled();
    });

    test('stops the axis as the first command to ensure that device is not moving and to clear possible warning flags', async () => {
      await runTestToEnd();

      expect(axis.stop).toHaveBeenCalled();
    });

    test('moves slowly when checking travel distance', async () => {
      await runTestToEnd();

      expect(axis.moveVelocity).toHaveBeenCalledWith(SAFE_APPROACH_SPEED_MM_PER_S, Velocity.MILLIMETRES_PER_SECOND);
    });

    test('displays orientation error when the move from home sensors fails quickly', async () => {
      axis.moveVelocity.mockImplementationOnce(() => {
        axis.waitUntilIdle.mockRejectedValueOnce(new MovementFailedException('Failed'));
        return Promise.resolve();
      });

      await runTestToEnd();

      wrapper.getByText(/Motor orientation appears to be incorrect/);
    });


    test('displays travel distance error when device travels move than heuristic limit and crashes', async () => {
      axis.position = 0;
      axis.moveVelocity.mockImplementationOnce(() => {
        axis.position = MAX_HOME_SENSOR_DISTANCE_HEURISTIC_MM + 1;
        axis.waitUntilIdle.mockRejectedValueOnce(new MovementFailedException('Failed'));
        return Promise.resolve();
      });

      fireEvent.click(wrapper.getByTestId('run-test'));
      await waitUntilPass(() => expect(selectTestRunning(store.getState())).toBe(false));

      wrapper.getByText(/The travel length that you entered seems too long\./);
    });
  });

  describe('cancellation', () => {
    test('issues stop and display cancellation message', async () => {
      const moveVelocityDefer = defer<void>();
      axis.moveVelocity.mockImplementation(() => moveVelocityDefer.promise);

      fireEvent.click(wrapper.getByTestId('run-test'));
      await waitUntilPass(() => expect(axis.moveVelocity).toHaveBeenCalled());

      expect(axis.stop).toHaveBeenCalledTimes(1);
      fireEvent.click(wrapper.getByTestId('stop-test'));

      await waitUntilPass(() => expect(selectTestRunning(store.getState())).toBe(false));
      moveVelocityDefer.resolve();

      expect(axis.stop).toHaveBeenCalledTimes(2);
    });

    test('sets the original approach speed when cancelled', async () => {
      const moveVelocityDefer = defer<void>();
      axis.moveVelocity.mockImplementation(() => moveVelocityDefer.promise);

      fireEvent.click(wrapper.getByTestId('run-test'));
      await waitUntilPass(() => expect(axis.moveVelocity).toHaveBeenCalled());
      fireEvent.click(wrapper.getByTestId('stop-test'));

      await waitUntilPass(() =>
        expect(axis.settings.set.mock.calls).toContainEqual(['limit.approach.maxspeed', ORIGINAL_APPROACH_SPEED])
      );
      moveVelocityDefer.resolve();
    });
  });

  test('displays success message', async () => {
    await runTestToEnd();

    wrapper.getByText(/Test Again/);
  });

  test('displays contact Zaber message if fails multiple times', async () => {
    axis.moveVelocity.mockImplementation(() => {
      axis.waitUntilIdle.mockRejectedValueOnce(new MovementFailedException('Failed'));
      return Promise.resolve();
    });

    await runTestToEnd();
    await runTestToEnd();

    expect(wrapper.queryAllByText(/contact Zaber support/)).toHaveLength(0);
    await runTestToEnd();
    wrapper.getByText(/contact Zaber support/);
  });
});
