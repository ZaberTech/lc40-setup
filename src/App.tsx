import React from 'react';
import { Provider } from 'react-redux';
import { Route, Switch } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import { Store } from 'redux';

import { Menu } from './menu';
import { ConnectDevices } from './connect_devices';
import { PhysicalAssembly } from './physical_assembly';
import { SplashScreen, DefaultRoute } from './splash_screen';
import { ConfigDevice } from './config_devices';
import { browserHistory } from './store/history';
import { Header, Link } from './components';
import { ErrorHandler } from './errors';
import { Feedback } from './feedback';
import { Footer } from './Footer';

import logoImg from './pictures/lc40_logo.svg';

export const App: React.FunctionComponent<{ store: Store }> = ({ store }) =>
  (<ErrorHandler>
    <Provider store={store}>
      <ConnectedRouter history={browserHistory}>
        <div className="layout root">
          <Header>
            <img src={logoImg} className="logo" alt="LC40 logo. A two-tone gear" />
            <Link to="/splash">LC40 Setup</Link>
          </Header>
          <div className="layout container">
            <Switch>
              <Route path="/splash" component={SplashScreen} />
              <Route>
                <div className="layout col menu">
                  <Menu />
                </div>
                <div className="layout col content">
                  <Switch>
                    <Route path="/physical-assembly" component={PhysicalAssembly} />
                    <Route path="/connect-devices" component={ConnectDevices} />
                    <Route path="/device-tuning">Device Tuning ToDo</Route>
                    <Route path="/feedback" component={Feedback} />
                    <Route path="/config-devices" component={ConfigDevice} />
                    <DefaultRoute />
                  </Switch>
                </div>
              </Route>
            </Switch>
          </div>
          <Footer/>
        </div>
      </ConnectedRouter>
    </Provider>
  </ErrorHandler>);

export default App;
