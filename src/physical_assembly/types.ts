export enum Steps {
  MOTOR_COUPLING = '/physical-assembly/motor-coupling',
  MOTOR_INSTALL = '/physical-assembly/motor-install',
  SENSOR_ASSEMBLY = '/physical-assembly/sensor-assembly',
  SENSOR_INSTALL = '/physical-assembly/sensor-install',
  DAISY_CHAIN = '/physical-assembly/setup-daisy-chain',
}
