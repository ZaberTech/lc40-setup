import { Steps } from './types';

export { PhysicalAssembly } from './PhysicalAssembly';
export const PHYSICAL_ASSEMBLY_PATH = Steps.MOTOR_COUPLING;
