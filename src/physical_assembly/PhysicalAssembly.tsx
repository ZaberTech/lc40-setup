import React from 'react';
import { Switch, Route, withRouter, RouteComponentProps } from 'react-router-dom';
import ReactTooltip from 'react-tooltip';

import { DotLink, DotGroup, Link, Button, TooltipText } from '../components';
import pulleyImg from '../pictures/coupling_assembly.png';
import motorImg from '../pictures/motor_assembly.png';
import hallAssemblyImg from '../pictures/hall_sensor_assembly.png';
import sensorInstallImg from '../pictures/limit_assembly.png';
import daisyImg from '../pictures/daisy.png';
import motorRight from '../pictures/motor_right_cropped.png';
import motorLeft from '../pictures/motor_left_cropped.png';
import { Content } from '../layout';

import { Steps } from './types';

enum Tooltips {
  LeftOrientation = 'LeftOrientation',
  RightOrientation = 'RightOrientation',
}

type Props = RouteComponentProps;

class PhysicalAssemblyBase extends React.Component<Props> {
  componentDidUpdate(previousProps: Props): void {
    if (this.props.location.pathname !== previousProps.location.pathname) {
      ReactTooltip.rebuild();
    }
  }

  render(): React.ReactNode {
    return (
      <Content className="physical-assembly">
        <DotGroup>
          <DotLink to={Steps.MOTOR_COUPLING} />
          <DotLink to={Steps.MOTOR_INSTALL} />
          <DotLink to={Steps.SENSOR_ASSEMBLY} />
          <DotLink to={Steps.SENSOR_INSTALL} />
          <DotLink to={Steps.DAISY_CHAIN} />
        </DotGroup>
        <Switch>
          <Route path={Steps.MOTOR_COUPLING}>
            <h3>Mount the clamping pulley coupling</h3>
            <div className="instruction-img" style={{ backgroundImage: `url(${pulleyImg})` }} />
            <p>
              Install the pulley coupling (AC183) flush with the end of the motor shaft.
              Tighten the clamping screw with a 2mm hex key to secure it tightly on the shaft.
            </p>
            <div className="button-group">
              <Link to="/connect-devices/select-port"><Button className="clear">Skip Physical Assembly</Button></Link>
              <Link to={Steps.MOTOR_INSTALL}><Button className="clear" rightarrow={true}>Next Step</Button></Link>
            </div>
          </Route>
          <Route path={Steps.MOTOR_INSTALL}>
            <h3>Install the motor</h3>
            <div className="instruction-img" style={{ backgroundImage: `url(${motorImg})` }} />
            <p>
              <b>Note:</b> The motor may be installed onto either end and either side in any orientation.
              The diagram above shows <TooltipText
                data-for="assembly-tooltip"
                data-tip={Tooltips.RightOrientation}>right side</TooltipText> mounting.
            </p>
            <p>
              To install the motor, remove any plastic pulley covers from the pulley hole with a 3mm hex key.
              Apply a drop of water around the pulley coupling for lubrication.
              Rotate and align the coupling with the inside of the pulley and press the motor into the hole.
              It may require firm steady pressure.
              Secure the motor with the M4 fasteners provided.
            </p>
            <div className="button-group">
              <Link to={Steps.MOTOR_COUPLING}><Button className="clear" leftarrow={true}>Previous Step</Button></Link>
              <Link to={Steps.SENSOR_ASSEMBLY}><Button className="clear" rightarrow={true}>Next Step</Button></Link>
            </div>
          </Route>
          <Route path={Steps.SENSOR_ASSEMBLY}>
            <h3>Assemble home sensor</h3>
            <div className="instruction-img" style={{ backgroundImage: `url(${hallAssemblyImg})` }} />
            <p>
              <b>Note: </b>The home sensor may be installed on either side of the LC40.
              See diagram A to install the sensor
              on the <TooltipText data-for="assembly-tooltip" data-tip={Tooltips.RightOrientation}>right side</TooltipText> and
              diagram B for the <TooltipText data-for="assembly-tooltip" data-tip={Tooltips.LeftOrientation}>left side</TooltipText>.
            </p>
            <p>
              Remove the set screw from the home sensor (HS02).
              Use the M3 button-head fastener to attach the sensor (HS02) to the sensor mount (AP187).
            </p>
            <div className="button-group">
              <Link to={Steps.MOTOR_INSTALL}><Button className="clear" leftarrow={true}>Previous Step</Button></Link>
              <Link to={Steps.SENSOR_INSTALL}><Button className="clear" rightarrow={true}>Next Step</Button></Link>
            </div>
          </Route>
          <Route path={Steps.SENSOR_INSTALL}>
            <h3>Install home sensor</h3>
            <div className="instruction-img" style={{ backgroundImage: `url(${sensorInstallImg})` }} />
            <p>
              Install the roll-in T-nut into the T-slot on the same side as the motor. This is where the home sensor will be mounted.
            </p>
            <p>
              Set the edge of the sensor mount at approximately 67 mm from the end of the extrusion. Fasten the
              home sensor mount in place using the T-nut and M6 fastener. Plug the sensor cable into the connector
              labeled “HOME” and thread on the locking collar.
            </p>
            <p>
              <b>Note:</b> The NMS23 motors are equipped to use an optional away sensor.
              The away sensor is identical to the home sensor and may be installed in the same manner,
              but it is not required for normal operation.
            </p>
            <div className="button-group">
              <Link to={Steps.SENSOR_ASSEMBLY}><Button className="clear" leftarrow={true}>Previous Step</Button></Link>
              <Link to={Steps.DAISY_CHAIN}><Button className="clear" rightarrow={true}>Next Step</Button></Link>
            </div>
          </Route>
          <Route path={Steps.DAISY_CHAIN}>
            <h3>Set up the daisy-chain</h3>
            <div className="instruction-img" style={{ backgroundImage: `url(${daisyImg})` }} />
            <p>
              If setting up more than one device, connect Zaber devices in a daisy-chain arrangement as shown in the diagram above.
            </p>
            <p>
              <b>Note: </b>
              We recommend connecting an X-DC02-NP cable between two devices which each will have their own power supply connected.
              This cable does not pass power along the daisy-chain, allowing each power supply to output up to its maximum current.
              If this cable is not used, the current draw of the devices may not be balanced between the power supplies.
            </p>
            <div className="button-group">
              <Link to={Steps.SENSOR_INSTALL}><Button className="clear" leftarrow={true}>Previous Step</Button></Link>
              <Link to="/connect-devices/select-port"><Button className="clear" rightarrow={true}>Device Connection</Button></Link>
            </div>
          </Route>
        </Switch>
        <ReactTooltip id="assembly-tooltip" className="tooltip"
          getContent={dataTip => {
            switch (dataTip) {
            case Tooltips.LeftOrientation:
              return <img src={motorLeft} alt="motor on the left side" />;
            case Tooltips.RightOrientation:
              return <img src={motorRight} alt="motor on the right side" />;
            }
          }} />
      </Content>
    );
  }
}

export const PhysicalAssembly = withRouter(PhysicalAssemblyBase);
