![Zaber Logo](docs/img/zaber-logo.png)

# Zaber LC40 Setup

Zaber LC40 Setup is an application that helps you assemble and set up your product.

## Download

Download the package from the link below depending on your Operating System:

### Windows

* [:floppy_disk: Installer](https://www.zaber.com/support/software-downloads.php?product=lc40_setup_win64_installer&version=latest) (x64, Installer)

### Linux

* [:floppy_disk: Ubuntu, Debian-based](https://www.zaber.com/support/software-downloads.php?product=lc40_setup_linux64_deb&version=latest) (x64, .deb package)

### macOS (Apple)

* [:floppy_disk: Installer](https://www.zaber.com/support/software-downloads.php?product=lc40_setup_darwin_dmg&version=latest) (x64, .dmg package)

## Compatible devices

* [LC40B-KM](https://www.zaber.com/products/linear-stages/LC40B-KM)

## Changelog

You can read our changelog [here](CHANGELOG.md).

## Support

Need assistance with the application?
Follow instructions on [zaber.com](https://www.zaber.com/contact) to contact our Applications Engineering Team.

Have feedback or suggestions for features?
Contact our Software Team by creating an [Issue](https://gitlab.com/ZaberTech/lc40-setup/issues).
