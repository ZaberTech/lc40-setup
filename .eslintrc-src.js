module.exports = {
  parser: '@typescript-eslint/parser',
  env: {
    'es6': true,
    'node': true
  },
  extends: [
    'eslint:recommended',
    'plugin:react/recommended',
    'plugin:@typescript-eslint/recommended',
    'react-app',
  ],
  parserOptions: {
    ecmaVersion: 2020,
    sourceType: 'module',
    ecmaFeatures: {
      jsx: true,
    }
  },
  settings: {
    react: {
      version: 'detect'
    }
  },
  rules: {
    'max-len': ['error', { code: 140 }],
    'indent': ['error', 2],
    'quotes': [
      'error',
      'single'
    ],
    'semi': [
      'error',
      'always'
    ],
    'camelcase': [
      'error'
    ],
    'prefer-const': [
      'error'
    ],
    'no-var': [
      'error'
    ],
    'object-curly-spacing': [
      'error',
      'always'
    ],
    'arrow-parens': [
      'error',
      'as-needed',
    ],
    'prefer-arrow-callback': [
      'error'
    ],
    'no-multiple-empty-lines': [
      'error'
    ],
    'no-cond-assign': [
      'error',
      'except-parens'
    ],
    'no-empty': 'off',
    '@typescript-eslint/explicit-module-boundary-types': 'off',
    '@typescript-eslint/no-inferrable-types': 'off',
    'react/prop-types': 'off',
    'jsx-a11y/anchor-is-valid': 'off',
  }
};
