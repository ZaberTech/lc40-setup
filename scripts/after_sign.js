const { notarize } = require('electron-notarize');
const { isRelease } = require('./utils');

exports.default = async function afterSign(context) {
  const { electronPlatformName, appOutDir } = context;
  if (electronPlatformName !== 'darwin' || !isRelease) {
    return;
  }

  const { productFilename, appId } = context.packager.appInfo;

  return await notarize({
    appBundleId: appId,
    appPath: `${appOutDir}/${productFilename}.app`,
    appleId: process.env.APPLEID,
    appleIdPassword: process.env.APPLEIDPASS,
  });
};
