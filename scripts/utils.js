const childProcess = require('child_process');

const isRelease = (process.env.CI_COMMIT_REF_NAME || '').match(/^release/i);

const exec = (...args) => new Promise((resolve, reject) => {
  const child = childProcess.exec(...args, err => err ? reject(err) : resolve());
  child.stdout.pipe(process.stdout);
  child.stderr.pipe(process.stderr);
});

module.exports = {
  exec,
  isRelease,
};
