module.exports = {
  preset: 'ts-jest',
  testEnvironment: 'jsdom',
  moduleNameMapper: {
    '.+\\.(png|jpg|ttf|woff|woff2)$': 'jest-transform-stub',
    '.+\\.(svg)$': '<rootDir>/scripts/mocks/svg_file.js',
  },
  setupFilesAfterEnv: ['./src/test/setup.ts'],
};
